---
id: july-7
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/decentralized-chat-protocols-with-unstoppable-domains-co-founder-and-ceo-bradley-kam/">Decentralized Chat Protocols with Unstoppable Domains Co-Founder and CEO Bradley Kam</a></h2>
<h4>July 7, 2020</h4>

<p>
<a href="https://www.linkedin.com/in/bradley-kam-444aa228/">Bradley Kam</a>, Co-Founder, and CRO at <a href="https://unstoppabledomains.com/">Unstoppable Domains</a>, joins <a href="https://hackernoon.com/u/Hackerhodl">Utsav Jaiswal</a> and <a href="https://hackernoon.com/u/arthur.tkachenko">Arthur Tkachenko</a> on the Hacker Noon Podcast to discuss how decentralization enables censorship-resistant peer to peer communication.
</p>
<a href="https://www.linkedin.com/in/bradley-kam-444aa228/">Bradley</a> discusses how decentralized registries such as their (dot)crypto lives outside the DNS naming system and how it cannot be pulled down by vested interests. He also shares his opinion on <a href="https://hackernoon.com/tagged/decentralization">decentralization</a> and <a href="https://hackernoon.com/tagged/security">security</a>.
<p>
<a href="https://www.linkedin.com/in/bradley-kam-444aa228/">Bradley Kam</a>’s speaks on the bylines of the launch of  <a href="https://unstoppabledomains.com/chat">Dchat</a>, which allows users to encrypt and store messages without the involvement of third parties. Dchat integrates  cryptocurrency wallets and a <a href="https://hackernoon.com/tagged/p2p">peer-to-peer (P2P</a>) storage network in order to securely store chat messages that are 100% controlled by users.
</p>
Continue reading about <a href="https://hackernoon.com/tagged/blockchain">blockchain</a>, <a href="https://hackernoon.com/tagged/security">security</a>, <a href="https://hackernoon.com/tagged/crypto">crypto</a> and <a href="https://hackernoon.com/tagged/startup">startups</a> on <a href="https://hackernoon.com/">Hacker Noon</a>.
Also, keep up <a href="https://hackernoon.com/">Hacker Noon</a> via the <a href="https://hackernoon.us19.list-manage.com/subscribe?u=b48b0ec2173fecf2586c00e80&id=fa796741e6">NOONIFICATION</a>, <a href="https://giphy.com/hackernoon">Giphy</a> (ahem <a href="https://facebook.com/hackernoon">Facebook </a>and/or <a href="https://www.instagram.com/hackernoon/">Instagram</a>), <a href="https://twitter.com/hackernoon">Twitter</a>, <a href="https://chrome.google.com/webstore/detail/tech-stories-tab-by-hacke/cmcgnandlhnjekeccmongfellpljlohh">the Tech Stories Chrome plugin</a>, or just <a href="https://publish.hackernoon.com/">get a technology story published today</a>. P.S. We also are <a href="http://sponsors.hackernoon.com/">accepting new sponsors.</a>