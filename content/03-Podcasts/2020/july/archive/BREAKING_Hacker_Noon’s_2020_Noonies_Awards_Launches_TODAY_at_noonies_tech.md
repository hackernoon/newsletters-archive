---
id: july-13
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/breaking-hacker-noon-s-2020-noonies-awards-launches-today-at-nooniestech/">BREAKING: Hacker Noon’s 2020 Noonies Awards Launches TODAY at noonies.tech</a></h2>
<h4>July 13, 2020</h4>

<p>
<strong>Join Hacker Noon’s Managing Editor, Natasha Nel, for a quick briefing on this year’s biggest, greenest and most independent tech industry awards: Hacker Noon’s prestigious #Noonies.<em> </em></strong>
</p>
<p>
<em>The 2020 #Noonies are made possible by Amplify Brokerage, the world’s first global zero trading fee cryptocurrency trading platform. GET STARTED: <a href="https://bit.ly/2O9xrzJ">https://bit.ly/2O9xrzJ</a> </em>
</p>
<em>.</em>
<p>
The original Noonies was conceptualized in early 2019, mostly as a ploy to distract our community from how many things were wrong with the site when we first moved off Medium in July of that same year. It turned into something bigger than we at Hacker Noon could’ve ever imagined.
</p>
<p>
This year, we’ve 10x’d the thing (sorry - touchy phrase in tech these days, I know) and the New Noonies includes two new award categories, and over 100 new award titles. In this quick listen, Natasha runs you through this year’s Noonies selection process; a few of her favourite awards, and how nominations and voting will work this year. Enjoy!
</p>
Nominate your best people and products in tech today: noonies.tech!