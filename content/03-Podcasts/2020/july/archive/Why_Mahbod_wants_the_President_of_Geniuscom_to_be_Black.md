---
id: july-9
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/the-next-president-of-genius-should-be-black-mahbod-moghadam-on-hacker-noon-podcast/">Why Mahbod wants the President of Genius.com to be Black</a></h2>
<h4>July 9, 2020</h4>

<p>
<a href="https://hackernoon.com/u/mahbodmoghadam">Mahbod Moghadam</a>, a Co-Founder of <a href="https://genius.com/">Rap Genius</a>, a founding member of <a href="https://everipedia.org/">Everipedia</a>, a Yale and Stanford alumni, joins <a href="https://www.linkedin.com/in/utsavjaiswal/">Utsav Jaiswal</a> on the Hacker Noon podcast to discuss the need for more black leadership and why the CEO of biggest black-culture American website needs to be black.
</p>
<a href="https://hackernoon.com/u/mahbodmoghadam">Mahbod</a> shares how his passion for <a href="https://hackernoon.com/tagged/music">music</a> and <a href="https://hackernoon.com/tagged/education">education</a> in high school influenced his future career. He shares how Genius started off as Rap Exegesis as an overnight project and grew to become the hip-hop torch-bearer it is today, worldwide.
<p>
They also discuss the need for black leadership, racism in the tech industry, and the role of cryptocurrency in reducing inequalities in societies. 
</p>
Mahbod ends with his opinion on Kanye West's decision to run for the president of the United States and a promise to campaign for Ye.
<p>
Continue reading about <a href="https://hackernoon.com/tagged/leadership">leadership</a>,<a href="https://hackernoon.com/tagged/blacklivesmatter"> black culture</a>, <a href="https://hackernoon.com/tagged/latest-tech-stories">tech stories</a>, and <a href="https://hackernoon.com/tagged/startup">startups</a> on Hacker Noon.
</p>
Also check <a href="https://hackernoon.com/u/mahbodmoghadam">Mahbod’s</a> latest stories on Hacker Noon:
<p>
<a href="https://hackernoon.com/the-ceo-of-my-company-genius-must-be-black-8q1030xa">The CEO of my company, Genius, must be Black</a>
</p>
<a href="https://hackernoon.com/genius-v-google-the-founder-of-genius-100-unbiased-take-and-perspectives-86a025094af2">GENIUS V. GOOGLE: The Founder of Genius’ 100% Unbiased Take and Perspectives</a>
Check out <a href="https://hackernoon.com/">Hacker Noon</a> via the <a href="https://hackernoon.us19.list-manage.com/subscribe?u=b48b0ec2173fecf2586c00e80&id=fa796741e6">NOONIFICATION</a>, <a href="https://giphy.com/hackernoon">Giphy</a> (ahem <a href="https://facebook.com/hackernoon">Facebook </a>and/or <a href="https://www.instagram.com/hackernoon/">Instagram</a>), <a href="https://twitter.com/hackernoon">Twitter</a>, or just <a href="https://publish.hackernoon.com/">get a technology story published today</a> and <a href="https://sponsor.hackernoon.com/">check our sponsorship</a> options.