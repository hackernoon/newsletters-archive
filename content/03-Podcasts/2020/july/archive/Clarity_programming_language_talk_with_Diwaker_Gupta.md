---
id: july-22
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/clarity-programming-language-talk-with-diwaker-gupta-aaron-blankstein-maurice-herlihy-and-alberto-cuesta-canada/">Clarity programming language talk with Diwaker Gupta, Aaron Blankstein, Maurice Herlihy and Alberto Cuesta Canada</a></h2>
<h4>July 22, 2020</h4>

<p>
<a href="https://www.linkedin.com/in/utsavjaiswal/">Utsav Jaiswal</a> hosts <a href="https://www.linkedin.com/in/diwakergupta/">Diwaker Gupta</a>, Head of Engineering at Blockstack, <a href="https://aaron.blankstein.com/">Aaron Blankstein</a>, Engineer at Blockstack, <a href="https://twitter.com/mpherlihy?lang=en">Maurice Herlihy</a>, Engineer and Advisor at Algorand and <a href="https://hackernoon.com/u/albertocuestacanada">Alberto Cuesta Canada</a>, Lead Blockchain Instructor at <a href="https://twitter.com/bskillstraining?lang=en">BeyondSkills</a> and a Hacker Noon contributor to talk everything about Clarity, a new smart contracting language. Hacker Noon’s best contributing writers discuss the need of new programming language by Blockstack and Algorand, how feasible it will be, what improvements and adjustments of Clarity to the existing ecosystem and more. 
</p>
 
<p>
Continue reading about <a href="https://hackernoon.com/tagged/blockstack">blockstack</a>, <a href="https://hackernoon.com/tagged/programming">programming</a>, <a href="https://hackernoon.com/tagged/decentralization">decentralization</a> and <a href="https://hackernoon.com/tagged/smart-contracts">smart contracts</a> on Hacker Noon.
</p>
 
<p>
Check our annual Noonies Awards nominees <a href="https://noonies.tech/">https://noonies.tech/</a>
</p>
Nominations are now open until August 13th
Also, check out <a href="https://hackernoon.com/">Hacker Noon</a> via the <a href="https://hackernoon.us19.list-manage.com/subscribe?u=b48b0ec2173fecf2586c00e80&id=fa796741e6">NOONIFICATION</a>, <a href="https://giphy.com/hackernoon">Giphy</a> (ahem <a href="https://facebook.com/hackernoon">Facebook </a>and/or <a href="https://www.instagram.com/hackernoon/">Instagram</a>), <a href="https://twitter.com/hackernoon">Twitter</a>, or just <a href="https://publish.hackernoon.com/">get a technology story published today</a> and <a href="https://sponsor.hackernoon.com/">check our sponsorship</a> options.