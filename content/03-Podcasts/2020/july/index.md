---
id: july-2020
title: July 2020
sidebar_label: July
---

- [Decentralized Chat Protocols with Unstoppable Domains Co-Founder and CEO Bradley Kam](archive/july-7)
- [Who is Linh Dao Smooke? A conversation about Entrepreneurship, Leadership and the Impact of Technology and Education with the Hacker Noon COO](archive/july-8)
- [Why Mahbod wants the President of Genius.com to be Black](archive/july-9)
- [BREAKING: Hacker Noon’s 2020 Noonies Awards Launches TODAY at noonies.tech](archive/july-13)
- [Clarity programming language talk with Diwaker Gupta, Aaron Blankstein, Maurice Herlihy and Alberto Cuesta Canada](archive/july-22)