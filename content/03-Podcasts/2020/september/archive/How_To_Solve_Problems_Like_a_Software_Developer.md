---
id: september-22
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/how-to-solve-problems-like-a-software-developer/">How To Solve Problems Like a Software Developer</a></h2>
<h4>September 22, 2020</h4>

<p>
Problem solving — technologists do it differently.™ <a href="https://hackernoon.com/u/natasha">Natasha Nel</a> asks 5 top <a href="https://hackernoon.com/">Hacker Noon</a> Contributors to share their secret <a href="https://hackernoon.com/tagged/problem-solving">problem-solving</a> frameworks, so that we can all learn to <a href="https://hackernoon.com/tagged/coding">think like devs</a>, during a time in which troubleshooting skills couldn't be more coveted.
</p>
<strong>With thanks to our 2020 Noonies award sponsors: <em><a href="https://bit.ly/38KPQMB">Sustany Capital</a>, <a href="https://bit.ly/38OYGce">.TECH Domains</a>, <a href="https://bit.ly/3dPJgWk">Grant for the Web</a>, <a href="https://bit.ly/2P3b1At">Skillsoft</a>, <a href="https://bit.ly/3gNQoUY">Flipside Crypto</a>, <a href="https://bit.ly/2ZmoeKD">Udacity</a>, and <a href="https://bit.ly/2Zh2q1O">Beyondskills</a>.</em></strong>
<p>
<strong>IN THIS PODCAST:</strong>
</p>
<p>
🎬 (02:55) <a href="https://hackernoon.com/u/rishabh">Rishabh Anand</a> on why a whiteboard helps with problem solving, and finding your own path forward
</p>
<p>
📝 (05:38) <a href="https://hackernoon.com/u/ryandawsonuk">Ryan Dawson</a> on figuring out the business objectives behind problems, the agile method, and seeking out meaningful feedback
</p>
<p>
🐛 (06:46) <a href="https://hackernoon.com/u/pizzapanther">Paul Bailey</a> with two frameworks for debugging — as a programmer —as well as an AI named Jane
</p>
<p>
💠 (10:55) <a href="https://hackernoon.com/u/summer.of.90s">Aditi Bhatnagar</a> on doing the research and connecting the dots
</p>
<p>
🍔 (12:48) <a href="https://hackernoon.com/u/alexeygrigorev">Alexey Grigorev</a> on the MoSCoW method, the 80/20 rule, and much, much more
</p>
 
<p>
<strong>READ MORE:</strong>
</p>
<p>
⚡ <a href="https://hackernoon.com/">hackernoon.com</a>
</p>
<p>
<strong>VOTE IN THE 2020 NOONIES:</strong>
</p>
<p>
👽 <a href="https://noonies.tech/">noonies.tech</a> 
</p>