---
id: september8
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/technology-trends-for-2021-a-forecast-from-hacker-noon-s-top-writers/">Technology Trends for 2021: A Forecast from Hacker Noon’s Top Writers</a></h2>
<h4>September 8, 2020</h4>

<p>
Natasha Nel asks 10 of Hacker Noon’s Top Contributors to weigh in with their strategic tech predictions for 2021 — expect a briefing on everything from recession startups and (self) edtech to AI applied to end Covid-19 and the commoditization of Machine Learning.
</p>
<strong>With thanks to our 2020 Noonies award sponsors: <em><a href="https://bit.ly/38KPQMB">Sustany Capital</a>, <a href="https://bit.ly/38OYGce">.TECH Domains</a>, <a href="https://bit.ly/3dPJgWk">Grant for the Web</a>, <a href="https://bit.ly/2P3b1At">Skillsoft</a>, <a href="https://bit.ly/3gNQoUY">Flipside Crypto</a>, <a href="https://bit.ly/2ZmoeKD">Udacity</a>, and <a href="https://bit.ly/2Zh2q1O">Beyondskills</a>.</em></strong>
<br/>
<strong>IN THIS PODCAST: </strong>
<ul>

<li><strong><a href="https://hackernoon.com/u/KleinKleinKlein">Matt Klein</a> on the state of <a href="https://hackernoon.com/tagged/mental-health">mental health (tech)</a> in America</strong>
</li>
<li><strong><a href="https://hackernoon.com/u/lblewisauthor">Melinda LB Lewis</a> on leveling up community and humanity in <a href="https://hackernoon.com/tagged/startup-culture">startup culture</a></strong>
</li>
<li><strong><a href="https://hackernoon.com/u/ryandawsonuk">Ryan Dawson</a> with real-world examples of how <a href="https://hackernoon.com/tagged/ethics">ethics is affecting commerce</a></strong>
</li>
<li><strong><a href="https://hackernoon.com/u/th3n00bc0d3r">Muhammed Bilal</a> on the collapse of traditional <a href="https://hackernoon.com/tagged/education">education</a> </strong>
</li>
<li><strong><a href="https://hackernoon.com/u/brianwallace">Brian Wallace</a> on why we need Chiefs of <a href="https://hackernoon.com/tagged/soft-skills">Soft Skills</a> </strong>
</li>
<li><strong><a href="https://hackernoon.com/u/spidertian">Tian Zhao</a> on <a href="https://hackernoon.com/tagged/web3">Web 3.0</a> and technology solving the problems of <a href="https://hackernoon.com/tagged/technology">technology</a></strong>
</li>
<li><strong><a href="https://hackernoon.com/u/gajesh2007">Gajesh Naik</a> on <a href="https://hackernoon.com/tagged/healthtech">healthtech</a> and the early detection of disease</strong>
</li>
<li><strong><a href="https://hackernoon.com/u/sharmi1206">Sharmistha Chatterjee</a> on AI and the implementation a <a href="https://hackernoon.com/tagged/covid19">Covid-19 vaccine</a> </strong>
</li>
<li><strong><a href="https://hackernoon.com/u/alexeygrigorev">Alexey Grigorev</a> on the <a href="https://hackernoon.com/tagged/no-code">no-code movement</a>, applied to <a href="https://hackernoon.com/tagged/machine-learning">Machine Learning</a></strong>
</li>
<li><strong><a href="https://hackernoon.com/u/RossPeili">Vladimiros Peilivanidis</a> on <a href="https://hackernoon.com/tagged/5g">5G</a>, <a href="https://hackernoon.com/tagged/technopolitics">technopolitics</a> and <a href="https://hackernoon.com/tagged/surveillance">surveillance</a></strong>
</li>
</ul>