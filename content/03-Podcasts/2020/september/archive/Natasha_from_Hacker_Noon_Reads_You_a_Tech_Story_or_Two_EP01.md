---
id: september-4
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/natasha-from-hacker-noon-reads-you-a-tech-story-or-two-%c2%b7-ep01/">Natasha from Hacker Noon Reads You a Tech Story or Two · EP01</a></h2>
<h4>September 4, 2020</h4>

<p>
This week’s top tech stories on <a href="https://hackernoon.com/">hackernoon.com</a> cover everything from robots to unicorns. Tune in for a 6-min summary from Natasha Nel, Managing Editor at Hacker Noon; read hackernoon.com for the full stories.
</p>
IN THIS PODCAST:
<ul>

<li>🤖 <strong><a href="https://hackernoon.com/the-rise-of-robots-insights-into-the-global-robotics-market-374g3u2q">Insights Into the Global Robotics Market</a></strong>
</li>
<li>🐫 <strong><a href="https://hackernoon.com/re-new-normal-todays-thriving-startups-will-be-camels-not-unicorns-fa2b3u9z">Today's Thriving Startups Will Be Camels, Not Unicorns</a></strong>
</li>
<li>✊🏿 <strong><a href="https://hackernoon.com/diversity-in-tech-3-actionable-steps-for-more-inclusive-tech-teams-i3p3tfs">3 Actionable Steps for More-Inclusive Tech Teams</a></strong>
</li>
<li>🤝 <strong><a href="https://hackernoon.com/how-to-negotiate-everything-in-life-czn3eza?source=rss">How to Negotiate Everything in Life</a></strong>
</li>
<li>🤯 <strong><a href="https://hackernoon.com/how-to-not-attribute-to-malice-things-adequately-explained-by-stupidity-5n393uh5">How to Not Attribute to Malice Things Adequately Explained by Stupidity</a></strong>
</li>
<li>👧 <strong><a href="https://hackernoon.com/how-i-started-my-own-business-at-19-u3183unp?source=rss">How I Started My Own Business at 19</a></strong>
</li>
Read the full stories: <a href="https://hackernoon.com/">hackernoon.com</a>
</ul>