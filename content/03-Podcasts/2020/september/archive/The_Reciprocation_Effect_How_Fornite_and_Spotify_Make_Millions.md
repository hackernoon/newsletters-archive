---
id: september-25
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/the-reciprocation-effect-how-fornite-and-spotify-make-millions/">The Reciprocation Effect: How Fornite and Spotify Make Millions</a></h2>
<h4>September 25, 2020</h4>

<p>
In this podcast, Limarc talks to Microsoft Engineer and Noonies Nominee Nataraj Sindam. In part 1 of this podcast, Nataraj and Limarc talk about The Freemium Model and The Reciprocation Effect, a pricing strategy used by some of the world’s largest companies, including Fortnite and Spotify. 
</p>
<strong>With thanks to our 2020 Noonies award sponsors: <em><a href="https://bit.ly/38KPQMB">Sustany Capital</a>, <a href="https://bit.ly/38OYGce">.TECH Domains</a>, <a href="https://bit.ly/3dPJgWk">Grant for the Web</a>, <a href="https://bit.ly/2P3b1At">Skillsoft</a>, <a href="https://bit.ly/3gNQoUY">Flipside Crypto</a>, <a href="https://bit.ly/2ZmoeKD">Udacity</a>, and <a href="https://bit.ly/2Zh2q1O">Beyondskills</a>.</em></strong>
<p>
Don’t forget to <a href="https://noonies.tech/award/hacker-noon-contributor-of-the-year-business-strategy">vote for Nataraj</a> in the 2020 Noonie Awards. 
</p>
 
<strong>Covered in Part 1 of this podcast: </strong>
<ul>

<li>Benefits of The Freemium Model
</li>
<li>What is the Reciprocation Effect?
</li>
</ul>
<strong>READ MORE ON HACKERNOON.COM: </strong>
<ul>

<li><a href="https://hackernoon.com/tagged/business">https://hackernoon.com/tagged/business</a>
</li>
<li><a href="https://hackernoon.com/tagged/freemium">https://hackernoon.com/tagged/freemium</a>
</li>
<li><a href="https://hackernoon.com/psychological-reason-behind-the-success-of-freemium-model-udeg3z9i">The Freemium Model's Reciprocation Effect</a> by Nataraj Sindam
</li>
</ul>

<strong>FOLLOW NATARAJ ON SOCIAL MEDIA:</strong>
<ul>

<li><a href="https://www.linkedin.com/in/natarajsindam/">https://www.linkedin.com/in/natarajsindam/</a>
</li>
<li><a href="https://twitter.com/natarajsindam">https://twitter.com/natarajsindam</a>
</li>
</ul>

<strong>CHECK OUT NATARAJ’S END2END PODCAST:</strong>
<ul>

<li><a href="https://open.spotify.com/show/39sihqVOYpq60z8UA6FYbw">https://open.spotify.com/show/39sihqVOYpq60z8UA6FYbw</a>
</li>
<li><a href="https://podcasts.apple.com/us/podcast/end-2-end/id1482225169">https://podcasts.apple.com/us/podcast/end-2-end/id1482225169</a>
</li>
</ul>