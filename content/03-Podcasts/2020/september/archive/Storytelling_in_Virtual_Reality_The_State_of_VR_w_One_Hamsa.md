---
id: september-8
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/storytelling-in-virtual-reality-the-state-of-vr-wone-hamsa/">Storytelling in Virtual Reality - The State of VR w/One Hamsa</a></h2>
<h4>September 8, 2020</h4>

<p>
In this podcast, Limarc talks to VR video game developers Assaf Ronen and Dave Levy from One Hamsa about storytelling in virtual reality and the state of VR in 2020. Assaf and Dave talk about their success on the Oculus Quest with their futuristic racquetball VR game, Racket NX, as well as their upcoming game which aims to bring effective storytelling mechanics to the VR medium.
</p>
<strong>Covered in this interview: </strong>
<ul>

<li>How One Hamsa began
</li>
<li>Why it is an advantage to develop games for the VR market
</li>
<li>Tips for VR game developers on choosing innovative mechanics
</li>
<li>The state of VR in 2020
</li>
<li>What needs to change for VR to thrive
</li>
<li>Oculus Quest forcing users to create a Facebook account</li>
</ul>
<strong>READ MORE ON HACKERNOON.COM: </strong>
<ul>

<li><a href="https://hackernoon.com/tagged/virtual-reality">https://hackernoon.com/tagged/virtual-reality</a>
</li>
<li><a href="https://hackernoon.com/tagged/virtualreality">https://hackernoon.com/tagged/virtualreality</a>
</li>
<li><a href="https://hackernoon.com/tagged/virtual-reality-technology">https://hackernoon.com/tagged/virtual-reality-technology</a>
</li>
<li><a href="https://hackernoon.com/tagged/augmented-reality">https://hackernoon.com/tagged/augmented-reality</a></li>
</ul>
<strong>SIGN UP FOR ONE HAMSA’S NEWSLETTER VIA THEIR WEBSITE:</strong>
<ul>

<li><a href="https://www.onehamsa.com/">https://www.onehamsa.com/</a>
</li>
</ul>