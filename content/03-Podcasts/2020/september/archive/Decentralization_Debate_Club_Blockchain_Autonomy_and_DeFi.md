---
id: september-21
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/decentralization-debate-club-blockchain-autonomy-and-defi/">Decentralization Debate Club: Blockchain, Autonomy, and DeFi</a></h2>
<h4>September 21, 2020</h4>

<p>
Has what started with Satoshi spun off into an hype-driven, ICO-indulgent startup industry of solutions for nothing? Or is blockchain technology and the mass decentralization it makes possible still worth the hype? Answered in 5 minutes by two top Hacker Noon Contributors — Mario Alves and Vladimiros Peilivanidis — tune in for a Decentralization Debate Club episode with Natasha Nel.
</p>
<strong>With thanks to our 2020 Noonies award sponsors: <em><a href="https://bit.ly/38KPQMB">Sustany Capital</a>, <a href="https://bit.ly/38OYGce">.TECH Domains</a>, <a href="https://bit.ly/3dPJgWk">Grant for the Web</a>, <a href="https://bit.ly/2P3b1At">Skillsoft</a>, <a href="https://bit.ly/3gNQoUY">Flipside Crypto</a>, <a href="https://bit.ly/2ZmoeKD">Udacity</a>, and <a href="https://bit.ly/2Zh2q1O">Beyondskills</a>.</em></strong>
READ MORE —
<ul>

<li><a href="https://hackernoon.com/tagged/decentralization">decentralization stories</a>
</li>
<li><a href="https://hackernoon.com/tagged/blockchain">blockchain stories</a>
</li>
<li><a href="https://hackernoon.com/tagged/cryptocurrency">crypto stories</a>
</li>
</ul>

<p>
ALL ON <a href="https://hackernoon.com/">HACKERNOON.COM</a> 💸
</p>