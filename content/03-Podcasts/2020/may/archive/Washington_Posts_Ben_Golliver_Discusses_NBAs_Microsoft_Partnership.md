---
id: may-1
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/washington-post-s-ben-golliver-discusses-nba-s-microsoft-partnership-kawhi-s-logo-lawsuit-and-the-modern-reporter-s-techstack/">Washington Post’s Ben Golliver Discusses NBA’s Microsoft Partnership, Kawhi’s Logo Lawsuit, and the Modern Reporter’s Techstack</a></h2>
<h4>May 1, 2020</h4>
<p>
Washington Post NBA Reporter <a href="https://www.google.com/search?q=Ben+Golliver&rlz=1C5CHFA_enVN829VN829&oq=Ben+Golliver&aqs=chrome..69i57j69i59l2j69i60l2.289j0j1&sourceid=chrome&ie=UTF-8">Ben Golliver</a> joins <a href="https://hackernoon.com/u/David">David Smooke</a> on the  <a href="https://podcast.hackernoon.com/">Hacker Noon podcast </a>to discuss Microsoft winning the NBA deal, what the Kawhi Leonard logo lawsuit means for the shoe industry, how the reporter’s tech stack has evolved over his career, what talents and traits lead to quality front office decision makers, the path from blogging to mainstream media, how the pandemic affected sports podcast advertising budgets, ways NBA players can make more money off the court, what it’s like to break Lebon James’ controversial China statements, the future of monetizing micro-content, the possibility of NBA players holding stock in their franchises, the emerging long tail seo of podcast listenership, and of course, how much fun it is to watch Zion hoop (“If LeBron is the athlete of Twitter, Zion is the NBA athlete of Instagram”). Also check out Ben’s <a href="https://goat.supportingcast.fm/">G.O.A.T</a> and <a href="https://www.si.com/nba/2019/03/26/si-nba-open-floor-podcast">Open Floor</a> podcasts. 
</p>
Some related reads on <a href="https://hackernoon.com/">Hacker Noon</a>:
<ul>

<li><a href="https://hackernoon.com/what-the-nba-can-teach-us-about-startups-27505e7ec03">What the NBA Can Teach Us About Startups</a>
</li>
<li><a href="https://hackernoon.com/esports-1b-teams-nba-in-the-1980s-b3ffcb54c4f7">Esports: $1B teams? (NBA in the 1980s)</a>
</li>
<li><a href="https://hackernoon.com/making-up-the-best-team-in-nba-history-qfey32ei">Making Up the Best Team in NBA History</a>
</li>
<li><a href="https://hackernoon.com/search?query=podcast">More Tech Podcast Insights</a>
</li>
</ul>