---
id: may-8
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/open-orchard-s-andrew-levine-discusses-decentralization-distributed-computing-and-enabling-developers-to-build-dapps-faster/">Open Orchard’s Andrew Levine Discusses Decentralization, Distributed Computing, and Enabling Developers To Build dApps Faster</a></h2>
<h4>May 8, 2020</h4>

<p>
CEO of Open Orchard, Ex Head of Communications & Advocacy at Steemit <a href="https://twitter.com/andrarchy">Andrew Levine</a> joins <a href="https://hackernoon.com/u/Hackerhodl">Utsav Jaiswal</a> to discuss whither blockchain, what inspired Andrew and his team at Open Orchard to start their project on launching new Blockchain Koinos, the driving philosophy as well as the underlying technology. Andrew talks about the differences between Bitcoin, Ethereum, Hiveblocks and Koinos Blockchains, how the Koinos will enable developers build their own dApps faster. Community's questions asked via #HackerNoonKoinos, related to explaining Blockchain to those not in the know, how success and knowledge are not as intertwined, the role of open source community and technology stalwarts in the adoption of niche technologies, and why the best way to boost progress in the world is to make it easier for everyone to launch more powerful applications.
</p>
Check related reads on Hacker Noon:
<p>
<a href="https://hackernoon.com/inside-trons-steem-takeover-attempt-and-the-birth-of-the-hive-blockchain-ya1g63249">Inside Tron's Steem Takeover Attempt and the Birth of the Hive Blockchain</a>
</p>
<a href="https://hackernoon.com/tagged/open-source">https://hackernoon.com/tagged/open-source</a>
<a href="https://hackernoon.com/tagged/blockchain">https://hackernoon.com/tagged/blockchain</a>