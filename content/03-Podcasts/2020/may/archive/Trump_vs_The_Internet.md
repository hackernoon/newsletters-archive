---
id: may-28
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/trump-vs-the-internet/">Trump vs. The Internet</a></h2>
<h4>May 28, 2020</h4>

<p>
What just happened to the internet!? Did our President just sign an executive order that forces social media platforms to be his personal mega phone? <a href="https://hackernoon.com/">Hacker Noon</a> <a href="https://hackernoon.com/u/Dane">CPO Dane Lyons</a> and <a href="https://hackernoon.com/u/David">CEO David Smooke</a> discuss the possible implications on 45's attempt to change internet law. Some sources discussed in this episode:
</p>
<ul>

<li><a href="https://www.eff.org/issues/cda230">Section 230 of the Communications Decency Act</a>
</li>
<li><a href="https://twitter.com/realDonaldTrump/status/1265255835124539392?ref_src=twsrc%5Etfw">Trump's Tweet About Mail in Ballot Fraud</a>
</li>
<li><a href="https://twitter.com/i/events/1265330601034256384">Twitter's Fact Checking Page</a>
</li>
<li><a href="https://deadline.com/2020/05/donald-trump-twitter-social-media-1202944096/">FCC's Official Statement on Feasibility of Executive Order</a>
</li>
<li><a href="https://twitter.com/ACLU/status/1266017892308410368">ACLU with the Burn </a></li>
Read more about <a href="https://hackernoon.com/tagged/net-neutrality">Net Neutrality</a>, <a href="https://hackernoon.com/tagged/twitter">Twitter</a>, <a href="https://hackernoon.com/search?query=censorship">Censorship</a>, <a href="https://hackernoon.com/search?query=fact%20checking">Fact Checking</a>, and <a href="https://hackernoon.com/tagged/internet">the Internet</a> on <a href="https://hackernoon.com/">HackerNoon.com</a>.
</ul>