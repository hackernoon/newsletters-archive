---
id: may-2020
title: May 2020
sidebar_label: May
---

- [Washington Post’s Ben Golliver Discusses NBA’s Microsoft Partnership, Kawhi’s Logo Lawsuit, and the Modern Reporter’s Techstack](archive/may-1)
- [Open Orchard’s Andrew Levine Discusses Decentralization, Distributed Computing, and Enabling Developers To Build dApps Faster](archive/may-8)
- [Spotify Makes Exclusive Deal for All Joe Rogan Content - What’s Next for Internet Creators?](archive/may-19)
- [How to Recover from Losing Half Your Revenue While Testing Positive for Covid-19](archive/may19)
- [Trump vs. The Internet](archive/may-28)