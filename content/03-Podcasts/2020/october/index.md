---
id: october-2020
title: October 2020
sidebar_label: October
---

- [The Future of Tech Belongs to Those Who Build It](archive/october-2)
- [The Push vs Pull Life: Cons of Recommendation Systems](archive/october-4)
- [Business Blockchains: Whither Now?](archive/october-21)
- [Gaming on the Blockchain Becomes Larger Than Just Gaming](archive/october21)
- [Catching up with NIS America | How COVID has Changed the Gaming Industry](archive/october-28)