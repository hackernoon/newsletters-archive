---
id: october21
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/gaming-on-the-blockchain-becomes-larger-than-just-gaming/">Gaming on the Blockchain Becomes Larger Than Just Gaming</a></h2>
<h4>October 21, 2020</h4>

<p>
Idan Zuckerman is a co-Founder at Upland and talks to Hacker Noon about his experiences from developing Upland - A EOS-based MMOG (Massively Multiplayer Online Game). Joining Utsav as a co-Host is Reza Jafery, the Chief Blockchain Officer at Akoin.
</p>
 
<p>
They discuss the potential of Blockchain-based games, challenges faced, adoption strategies implemented, and key results obtained.
</p>
 
<p>
Check out Upland: https://twitter.com/uplandme
</p>
<p>
Check out Akoin: https://twitter.com/AkoinOfficial
</p>
 
<p>
Idan’s Twitter: https://twitter.com/idanzuck
</p>
 
<p>
Reza’s Twitter: <a href="https://twitter.com/RezaJafery">https://twitter.com/RezaJafery</a>
</p>
Utsav’s Twitter: <a href="https://twitter.com/utsav_jaiswal1">https://twitter.com/utsav_jaiswal1</a>
<p>
 Continue reading about <a href="https://hackernoon.com/tagged/blockchain">blockchain</a>, <a href="https://hackernoon.com/tagged/economics">economics</a>, and <a href="https://hackernoon.com/tagged/smart-contracts">smart contracts</a> on Hacker Noon.
</p>
 
<p>
Also, check out <a href="https://hackernoon.com/">Hacker Noon</a> via <a href="https://giphy.com/hackernoon">Giphy</a> (ahem <a href="https://facebook.com/hackernoon">Facebook </a>and/or <a href="https://www.instagram.com/hackernoon/">Instagram</a>), <a href="https://twitter.com/hackernoon">Twitter</a>, or just <a href="https://publish.hackernoon.com/">get a technology story published today</a> and <a href="https://sponsor.hackernoon.com/">check our sponsorship</a> options.
</p>