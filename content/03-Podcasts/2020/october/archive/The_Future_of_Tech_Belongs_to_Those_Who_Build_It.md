---
id: october-2
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/the-future-of-tech-belongs-to-those-who-build-it/">The Future of Tech Belongs to Those Who Build It</a></h2>
<h4>October 2, 2020</h4>

<p>
<strong>What are the best product development principles and processes for building a better internet?</strong> Those who design, decide. I may have trained an AI to host this podcast — but the tech industry insights inside are all 100% human. Tune in for a far-reaching conversation with me, Natasha Nel's AI alter-ego, and seven of <a href="https://hackernoon.com/">hackernoon.com</a>'s top contributors. 
</p>
<strong>With thanks to our 2020 Noonies award sponsors: <em><a href="https://bit.ly/38KPQMB">Sustany Capital</a>, <a href="https://bit.ly/38OYGce">.TECH Domains</a>, <a href="https://bit.ly/3dPJgWk">Grant for the Web</a>, <a href="https://bit.ly/2P3b1At">Skillsoft</a>, <a href="https://bit.ly/3gNQoUY">Flipside Crypto</a>, <a href="https://bit.ly/2ZmoeKD">Udacity</a>, and <a href="https://bit.ly/2Zh2q1O">Beyondskills</a>.</em></strong>
<p>
<strong>🎙️ IN THIS EPISODE:</strong>
</p>
· <a href="https://hackernoon.com/u/ryandawsonuk">Ryan Dawson</a> on trolls, misinformation, and online hate (02:45); why Wikipedia is a strong example of good product design principles (03:10); why other platforms struggle to police the quality of content posted on their sites (03:52); and which story he'd put on a billboard to demonstrate the negative potential of making decisions from a place of fear or narrow-mindedness (04:27).
<p>
· <a href="https://hackernoon.com/u/lblewisauthor">Melinda L.B Lewis</a> on what she'd put on a billboard (05:35) and why, paradoxically, disconnecting from technology is likely to make you better at engaging with it (06:05).
</p>
· <a href="https://hackernoon.com/u/sandra-shpilberg">Sandra Shpilberg</a> on the definition and importance of Single Deep Focus in the process of creating and developing (06:35) and the four simple steps you can take to achieve a Single Deep Focus state (07:44).
<p>
· <a href="https://hackernoon.com/u/alexeygrigorev">Alexey Grigorev</a> on ideation, talking to people, coming up with solutions (10:28); how to know when you've got a good blog post idea on your hands, and how to begin the writing process (11:18).
</p>
· <a href="https://hackernoon.com/u/benmmar">Benjamin Mmari</a> on staying connected in COVID-19 times and the risk of working in a silo (12:35);  when Zoom just isn't quite the same an in-person conferences (14:01); and how to overcome pandemic-induced obstacles to your productivity (15:09).
<p>
· <a href="https://hackernoon.com/u/rish-16">Rishabh Anand</a> on creating and writing things that add value; following a research-intensive ideation process (15:57); the tools and tactics of a visual learner, and defeating writer's block (17:09).
</p>
· <a href="https://hackernoon.com/u/yonatan">Yonatan Kagansky</a> on the influence the internet has on our brains; the risk of becoming puppets; going #BackToTheInternet (18:22); the most common mistake product developers are making today, and how to use the Story Test to determine product viability (19:31).
<p>
<em>With special thanks to <a href="https://hackernoon.com/u/RossPeili">Vladimiros Peilivanidis</a> for the introduction to this episode, and <a href="https://noonies.tech/">The 2020 Noonies</a> sponsors who made this all possible: <a href="https://bit.ly/38KPQMB">Sustany Capital</a>, <a href="https://bit.ly/38OYGce">.TECH Domains</a>, <a href="https://bit.ly/3dPJgWk">Grant for the Web</a>, <a href="https://bit.ly/2P3b1At">Skillsoft</a>, <a href="https://bit.ly/3gNQoUY">Flipside Crypto</a>, <a href="https://bit.ly/2ZmoeKD">Udacity</a>, and <a href="https://bit.ly/2Zh2q1O">Beyondskills</a>.</em>
</p>
<strong><a href="http://noonies.tech/">Vote in the 2020 Noonies before voting closes on 12 October!</a></strong>
<h3><strong><a href="https://hackernoon.com/">Read more on Hacker Noon</a> — it's how hackers start their afternoons. 🚀</strong></h3>