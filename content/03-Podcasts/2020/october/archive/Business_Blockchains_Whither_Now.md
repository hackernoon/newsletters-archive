---
id: october-21
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/business-blockchains-whither-now/">Business Blockchains: Whither Now?</a></h2>
<h4>October 21, 2020</h4>

<p>
Lars Rensing is the CEO at PROTOKOL and a co-Founder at ARK.io and talks to Hacker Noon about his experiences from developing PROTOKOL - to solve real business challenges through the power of custom blockchain solutions. Joining Lars is Justin Renken, the Senior Brand Manager at ARK.io.
</p>
 
<p>
They discuss making Blockchain accessible to corporations, the future of content marketing, and the challenges of building a Point, Click, Blockchain.
</p>
 
<p>
Check out PROTOKOL: https://www.protokol.com/
</p>
<p>
Check out ARK: https://ark.io/
</p>
 
<p>
Lars’ Twitter: https://twitter.com/l_rensing
</p>
Justin’s Podcast: https://podcast.ARK.io
<p>
Utsav’s Twitter: <a href="https://twitter.com/utsav_jaiswal1">https://twitter.com/utsav_jaiswal1</a>
</p>
 
<p>
Continue reading about <a href="https://hackernoon.com/tagged/blockchain">blockchain</a>, <a href="https://hackernoon.com/tagged/economics">economics</a>, and <a href="https://hackernoon.com/tagged/smart-contracts">smart contracts</a> on Hacker Noon.
</p>
 
<p>
Also, check out <a href="https://hackernoon.com/">Hacker Noon</a> via <a href="https://giphy.com/hackernoon">Giphy</a> (ahem <a href="https://facebook.com/hackernoon">Facebook </a>and/or <a href="https://www.instagram.com/hackernoon/">Instagram</a>), <a href="https://twitter.com/hackernoon">Twitter</a>, or just <a href="https://publish.hackernoon.com/">get a technology story published today</a> and <a href="https://sponsor.hackernoon.com/">check our sponsorship</a> options.
</p>