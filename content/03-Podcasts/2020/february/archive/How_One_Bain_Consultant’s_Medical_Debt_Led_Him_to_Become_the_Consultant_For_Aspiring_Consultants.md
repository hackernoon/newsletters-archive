---
id: february-3
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/how-one-bain-consultant-s-medical-debt-led-him-to-become-the-consultant-for-aspiring-consultants/">How One Bain Consultant’s Medical Debt Led Him to Become the Consultant For Aspiring Consultants</a></h2>
<h4>February 3, 2020</h4>

<p>
<strong>Listen to the interview on <a href="https://podcasts.apple.com/us/podcast/the-hacker-noon-podcast/id1436233955">iTunes</a>, or <a href="https://play.google.com/music/m/Dfuna5a4pzsmjr76bxabkxdrhim?t=Product_Iteration_with_Hacker_Noon_Interim_CTO_Dane_Lyons-Hacker_Noon_Podcast">Google Podcast</a>, or watch on <a href="https://www.youtube.com/channel/UChu5YILgrOYOfkfRlTB-D-g">YouTube</a>.</strong>
</p>
In this episode <a href="https://hackernoon.com/@David">David Smooke</a> interviews <a href="https://www.linkedin.com/in/davis2/">Davis Nguyen</a>, Founder of <a href="https://www.myconsultingoffer.org/">My Consulting Offer</a>.  They discuss Davis's way into <a href="https://hackernoon.com/tagged/consulting">consulting</a>, insights on <a href="https://hackernoon.com/tagged/social-media-marketing">social media marketing</a>, and people’s motivations behind <a href="https://hackernoon.com/tagged/startups">startups</a>.
<p>
<em>“I spent my money on just online courses. So I bought the best courses on YouTube ads, Facebook ads, Twitter ads, LinkedIn ads. What people spend, I imagine on alcohol and travelling, I've just spent on online courses.”</em>
</p>
<em>“ I know that maybe I am the person who gets us from zero to seven figures, which is there. But to go to eight figures and so forth in profit, that's either gonna by hiring a great team, training myself or just replacing myself eventually. Because I have no problem with that. It’s like how do we serve our customer base and how do we help people become better thinkers. And I'm biased because I work in management consulting, but I think that's one of the best training for anyone who wants to go down that road and they don't want to start a business, they are not like a side hustler or anything like that. And I want to make sure to continue that.”</em>
<p>
<em>- <a href="https://www.linkedin.com/in/davis2/">Davis Nguyen</a></em>
</p>
<em>P.S. If you dig the new <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>, consider <a href="https://itunes.apple.com/us/podcast/the-hacker-noon-podcast/id1436233955?mt=2">giving us a 5 star review on iTunes.</a></em>
<h4>Also check out <a href="http://hackernoon.com/">today’s homepage</a>.</h4>