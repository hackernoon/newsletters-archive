---
id: june-19
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/how-to-build-cryptocurrency-exchange-from-scratch-by-adam-todd-ceo-founder-of-digitex-futures-1592578686/">How to build cryptocurrency exchange from scratch by Adam Todd, CEO & Founder of Digitex Futures</a></h2>
<h4>June 19, 2020</h4>

<p>
<a href="https://www.linkedin.com/in/adam-todd-digitex/">Adam Todd</a>, Lead Developer and CEO of <a href="https://digitexfutures.com/">Digitex Futures Exchange</a>, joins <a href="https://hackernoon.com/u/Hackerhodl">Utsav Jaiswal</a> to share his journey in trading and discuss what it takes to build a cryptocurrency exchange. 
</p>
They discuss cryptocurrency space in general, bitcoin, and the effort behind building cryptocurrency exchange from scratch. Adam shares the story of his trading experience which started when he was 19, and how it led him to build a zero-fee cryptocurrency exchange.
<p>
Check related reads on <a href="https://hackernoon.com/tagged/digitex-futures/">Digitex</a>, <a href="https://hackernoon.com/tagged/trading">trading</a> and <a href="https://hackernoon.com/tagged/cryptocurrency">cryptocurrency</a> on <a href="https://hackernoon.com/">Hacker Noon</a>. 
</p>
Also check out <a href="https://hackernoon.com/">Hacker Noon</a> via the <a href="https://hackernoon.us19.list-manage.com/subscribe?u=b48b0ec2173fecf2586c00e80&id=fa796741e6">NOONIFICATION</a>, <a href="https://giphy.com/hackernoon">Giphy</a> (ahem <a href="https://facebook.com/hackernoon">Facebook </a>and/or <a href="https://www.instagram.com/hackernoon/">Instagram</a>), <a href="https://twitter.com/hackernoon">Twitter</a>, or just <a href="https://publish.hackernoon.com/">get a technology story published today</a>. 