---
id: april-8
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/holochain-deep-dive-with-arthur-brock-and-eric-haris-braun/">Holochain Deep Dive with Arthur Brock and Eric-Harris-Braun</a></h2>
<h4>April 8, 2020</h4>

<p>
<strong>Listen to the interview on <a href="https://podcasts.apple.com/us/podcast/the-hacker-noon-podcast/id1436233955">iTunes</a> or watch on <a href="https://www.youtube.com/channel/UChu5YILgrOYOfkfRlTB-D-g">YouTube</a>.</strong>
</p>
 
<p>
In this episode <a href="https://hackernoon.com/@Hackerhodl">Utsav Jaiswal</a> interviews <a href="https://twitter.com/artbrock?lang=en">Arthur Brock</a> and <a href="https://twitter.com/zippy314?lang=en">Eric Harris-Braun</a>, Co-Founders of <a href="https://holochain.org/">Holochain</a> - an Open Source framework for building fully distributed, peer-to-peer applications. 
</p>
They talk about <a href="https://hackernoon.com/tagged/holochain">Holochain</a>’s mission and values, how it differs from Blockchains, and how to manage distributed teams. 
 
<p>
<em>“What we've been interested in is really how do we collectively shape currents, flows for a better world, for greater collective intelligence, for coordinating and collaborating better.”</em>
</p>
<em> - <a href="https://twitter.com/artbrock?lang=en">Arthur Brock</a></em>
 
<p>
<em>“Holochain is what we've come to after a very long time and a lot of intense efforts in understanding those (biological) patterns of how to create tools for distributed collective intelligence... And Holochain is our answer to being able to do that.”</em>
</p>
- <a href="https://twitter.com/zippy314?lang=en">Eric Harris-Braun</a>
<p>
<em>“It creates currencies that are not speculated. It creates currencies that are based on actual production. People, for a long time, have been wondering why we have all these currencies when the government can issue as much of it as they want, whenever they want. </em>
</p>
<em>It's funny, in the blockchain world, they call their currencies non-fiat and government currencies - fiat, but blockchain currencies are also actually fiat. </em>
<p>
<strong><em>They are declared out of nowhere. </em></strong>
</p>
<em>Whereas, if you do this other pattern of creating mutual credit currencies based on productive capacity, then you are actually creating something that represents actual value in the world and <strong>our economics can become based on value rather than speculation</strong>.”</em>
<p>
- <a href="https://twitter.com/zippy314?lang=en">Eric Harris-Braun</a>
</p>
<em>P.S. If you dig the new <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>, consider <a href="https://itunes.apple.com/us/podcast/the-hacker-noon-podcast/id1436233955?mt=2">giving us a 5 star review on iTunes.</a></em>
<h4>Also check out <a href="http://hackernoon.com/">today’s homepage</a>.</h4>