---
id: march-27
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/linhdao-smookeandutsavjaiswal-discuss-decentralizationchallengesandsolutionswiththenem-foundations-president-alex-tinsmanand-nem-cto-nate-d-amico/">Linh Dao Smooke and Utsav Jaiswal discuss Decentralization, Challenges, and Solutions with The NEM Foundation’s President Alex Tinsman and NEM CTO Nate D’Amico</a></h2>
<h4>March 27, 2020</h4>

<p>
<strong>Listen to the interview on <a href="https://podcasts.apple.com/us/podcast/the-hacker-noon-podcast/id1436233955">iTunes</a> or watch on <a href="https://www.youtube.com/channel/UChu5YILgrOYOfkfRlTB-D-g">YouTube</a>.</strong>
</p>
In this episode <a href="https://hackernoon.com/@linh">Linh Dao Smooke</a> and <a href="https://hackernoon.com/@Hackerhodl">Utsav Jaiswal</a> interview <a href="https://twitter.com/Inside_NEM?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor">Alex Tinsman</a>, President at NEM.io Foundation and NEM’s CTO <a href="https://www.linkedin.com/in/kaiyzen/">Nate D’Amico</a>.  
<p>
They discuss cryptocurrency adoption, difference between NEM, a peer-to-peer cryptocurrency and blockchain platform, and Nem.IO Foundation, gender role in crypto tech spheres, blockchain decentralization, Nem’s operation mindset and the future of NEM. 
</p>
Interview was recorded on January 18th, 2020. 
<p>
<em>“What's pretty interesting about the trajectory with the path for the NEM is that it was so forward thinking with the security features that it has always overcome and remained when all these other coins on platforms kind of went away. I think it has a bright future with technology. Platforms like NEM stick around because they are forward thinking with their features and their abilities.”</em> 
</p>
<em>“I want to be a part of change and the only way to do so is to educate yourself. And the only way to educate yourself is to get dirty. To get dirty with the code, get dirty with the channels, understand. Get into the thick of it. And then it gets really exciting [blockchain and cryptocurrency) because it opens up the opportunity for anyone. Anyone, no matter what your skills set is, everyone has the same level of playing field.  So I don't really think of it as a female thing, I think of it as an empowering tool that anyone and everyone can be a part of. So to me it's not gender, it's a movement.” -</em>
<p>
<em><a href="https://twitter.com/Inside_NEM?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor">Alex Tinsman</a></em>
</p>
<em>“If someone on even local, city council level wanted to do a similar voting style that we are using for the NEM ecosystem, they could launch a private or public chain and it doesn't really matter as functionally of the network would be doing the same thing. They could launch a private chain or public chain and still make it auditable, so anyone could have a verifiable data that they would check and download. And then they could configure that network by just tuning a couple labs when they launch it to decide how the consensus works. So if you’re launching a six server node of private network for local city council loading and you’re gonna use it once and then archive it for historical verification purposes, and then every year stand up a new network - you could configure it in a very simplified way that gives you total control on how to protect the network.”</em>
<p>
<em><a href="https://www.linkedin.com/in/kaiyzen/">- Nate D'Amico</a></em>
</p>
<em>P.S. If you dig the new <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>, consider <a href="https://itunes.apple.com/us/podcast/the-hacker-noon-podcast/id1436233955?mt=2">giving us a 5 star review on iTunes.</a></em>