---
id: august-2020
title: August 2020
sidebar_label: August
---

- [How (and Why) to Build and Launch Products in 24hrs with Zoe Chew](archive/august-10)
- [Monica Quaintance and Lisa JY Tan on Public Blockchains — Design, Security, Economics, and Fairness](archive/august-19)
- [Building for Return on Environment with Product Strategist Dani Laity](archive/august-25)
- [How to Hack a Huge Career in Tech with PR Expert & Founder Sarah Evans](archive/august-31)