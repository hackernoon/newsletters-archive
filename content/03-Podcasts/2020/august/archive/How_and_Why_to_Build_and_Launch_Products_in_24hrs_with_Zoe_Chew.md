---
id: august-10
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/how-and-why-to-build-and-launch-products-in-24hrs-with-zoe-chew/">How (and Why) to Build and Launch Products in 24hrs with Zoe Chew</a></h2>
<h4>August 10, 2020</h4>

<p>
<strong>Natasha Nel interviews 6x Noonie Nominee*, <a href="http://hackernoon.com/">hackernoon.com</a> Contributor, and Prolific Maker, Zoe Chew, to talk Product, Tech, Startups, and Marketing.</strong>
</p>
 
<strong>*PUBLIC NOMS CLOSE AT NOON ON WED, 12 AUG:</strong> Last chance to get your best in tech (or yourself) nominated for some well-deserved industry recognition this year! Choose from over 200+ Tech Industry Awards and<strong> </strong><a href="https://noonies.tech/">Add Your Last-Minute Noonies Nominations at NOONIES.TECH</a><strong> today </strong>

<p id="gdcalert1" ><span> >>>>>  gd2md-html alert: inline image link here (to images/image1.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br/>(<a href="#">Back to top</a>)(<a href="#gdcalert2">Next alert</a>)<br/><span> >>>>> </span></p>


<img src="images/image1.png" width="" alt="alt_text" title="image_tooltip" />

<strong>THINGS COVERED IN THIS CONVERSATION:</strong>
<ul>

<li>How to transition from a marketing career to product building and consulting
</li>
<li>How to come up with good ideas for great products 
</li>
<li>How to build and launch products in 24 hours
</li>
<li>The value of keeping things simple
</li>
<li>How to keep yourself accountable to your goals and stop procrastinating
</li>
<li>Strategies for success as a solo founder
</li>
<li>Zoe’s 3-level approach to branding and marketing yourself as a tech professional / introvert
</li>
<li>How to stay productive and motivated working remotely
</li>
<li>And so much more!
</li>

</ul>
<p>
Read 
</p>
<a href="https://www.youtube.com/redirect?v=ri_CsaXa07w&redir_token=QUFFLUhqa0hiV0t5SUlMeXFZemgtOHFvZzlhblBFQzctUXxBQ3Jtc0tsRWs5WXNib0xKMHlEUWpzWXMxUmpTMTRlQ2pDZ1kwVWpPMThPVVEzYTBfUnVVU0FzMnBoMGhjWE5PdEp6QXBXdnhrdUJTaW52WUlreWtma2p0UGNpVGRVamZuYXNRcFBjRGFUMmRXR3JJZEV4YkxjVQ%3D%3D&event=video_description&q=https%3A%2F%2Fhackernoon.com%2Ftagged%2Fproduct">https://hackernoon.com/tagged/product</a>
<p>
<a href="https://www.youtube.com/redirect?v=ri_CsaXa07w&redir_token=QUFFLUhqbkRZWUEwZXl6UDJyb0Z0QkpBbUJvcU0yUU44QXxBQ3Jtc0ttWVhkd3F4NWV4ZDhNejk5OXdsekVPUTV5YVYyWkp5M3g0WUo2ckpWOGNDakp6dEc4UjBpT1R3M3BDV0ZCZHQ3WGpuSHdaODI4LVY0LU5oWmN0cWdnbW9oVFFldDNGTlAwcnhsRVlnOHZxMkJ1X1RNRQ%3D%3D&event=video_description&q=https%3A%2F%2Fhackernoon.com%2Ftagged%2Fstartups">https://hackernoon.com/tagged/startups</a> <a href="https://www.youtube.com/redirect?v=ri_CsaXa07w&redir_token=QUFFLUhqbXRQcDgzUklaX0l6MEpyUEdYbF95TndzTHBvZ3xBQ3Jtc0trU0U5d014a2lsSXdNQk15NF81T2lXMC1qUjBNYVdxS002WFRORU9HWWhpNFVGR09aMndrWW5wbFhiczRrZnZlRUNFYWtLZ3diVnZpMHd3bDdzWC1mRG9UUm1pMXJKYXhHdzdmVUJXczhuRFhJa014Yw%3D%3D&event=video_description&q=https%3A%2F%2Fhackernoon.com%2Ftagged%2Ftechnology">https://hackernoon.com/tagged/technology</a> <a href="https://www.youtube.com/redirect?v=ri_CsaXa07w&redir_token=QUFFLUhqbEdTQWVFY21HdmdVWFBXa1RtUDhId1BKV3lBUXxBQ3Jtc0tuVUF3Q1ctSW9ObDVFU1h1eGhSNTg4a2Z1NElLWlFQX1k3MklzUGJ2eWhtZzBNUHZGVkxMa1h5allYdkxhcDRDYm5LaXY2UmZLdWlGU3ZvMDVpWmRSVjM3aldJa3lvRDZ3RVhpUWlDZFhoVDZjS0Vwbw%3D%3D&event=video_description&q=https%3A%2F%2Fhackernoon.com%2Ftagged%2Fmarketing">https://hackernoon.com/tagged/marketing</a>
</p>
Keep up with Zoe Chew
<ul>

<li><a href="https://whizzoe.com/">https://whizzoe.com/</a>
</li>
<li><a href="https://twitter.com/whizzzoe/">https://twitter.com/whizzzoe/</a>
</li>
<li><a href="https://www.instagram.com/whizzoe/">https://www.instagram.com/whizzoe/</a>
</li>
<li><a href="https://www.producthunt.com/@whizzzoe/">https://www.producthunt.com/@whizzzoe/</a>
</li>
</ul>