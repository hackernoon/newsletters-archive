---
id: august-19
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/monica-quaintance-and-lisa-jy-tan-on-public-blockchains-%e2%80%94-design-security-economics-and-fairness/">Monica Quaintance and Lisa JY Tan on Public Blockchains — Design, Security, Economics, and Fairness</a></h2>
<h4>August 19, 2020</h4>

<p>
<a href="https://www.linkedin.com/in/monica-quaintance/">Monica Quaintance</a>, who is the Head of Research and Networks at Kadena Blockchain. She’s a former Senior Data Engineer at Rent the Runway and a former Quantitative Analytics Engineer at the SEC. Joining <a href="https://hackernoon.com/u/Hackerhodl">Utsav Jaiswal</a> from <a href="https://hackernoon.com/">Hacker Noon</a> as co-host is <a href="https://lisajytan.com/">Lisa JY Tan</a>, Founder and Lead Economist at <a href="https://www.economicsdesign.com/">Economics Design</a>.
</p>
They discuss Monica’s entry into Blockchain, the state of Blockchains in general and her contribution to the development of the <a href="https://www.kadena.io/kadena">Kadena Public Blockchain</a>, how it provides cross-chain transactions, smart contracts, and the myth of unhackable Blockchains. 
<p>
Monica also serves as a co-Founder at Universal Consensus, through which, she advocates for more diversity in the Blockchain sector to realize true universal consensus.
</p>
Continue reading about <a href="https://hackernoon.com/tagged/blockchain">blockchain</a>, <a href="https://hackernoon.com/tagged/economics">economics</a> and <a href="https://hackernoon.com/tagged/smart-contracts">smart contracts</a> on Hacker Noon.
 
<p>
Check our annual Noonies Awards nominees <a href="https://noonies.tech/">https://noonies.tech/</a>.
</p>
Voting is now live!
 
<p>
Also, check out <a href="https://hackernoon.com/">Hacker Noon</a> via the <a href="https://hackernoon.us19.list-manage.com/subscribe?u=b48b0ec2173fecf2586c00e80&id=fa796741e6">NOONIFICATION</a>, <a href="https://giphy.com/hackernoon">Giphy</a> (ahem <a href="https://facebook.com/hackernoon">Facebook </a>and/or <a href="https://www.instagram.com/hackernoon/">Instagram</a>), <a href="https://twitter.com/hackernoon">Twitter</a>, or just <a href="https://publish.hackernoon.com/">get a technology story published today</a> and <a href="https://sponsor.hackernoon.com/">check our sponsorship</a> options.
</p>