---
id: august-25
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/building-for-return-on-environment-with-product-strategist-dani-laity/">Building for Return on Environment with Product Strategist Dani Laity</a></h2>
<h4>August 25, 2020</h4>

<p>
Natasha asks Dani Laity of Aurora Sustainability to unpack the sustainable tech product strategies and monetization models powering Aurora Sustainability—2020 Noonie Nominee in for <strong><a href="https://noonies.tech/award/best-use-of-tech-for-good">Best Use of Tech for Good</a></strong> in <a href="http://hackernoon.com/">hackernoon.com</a>’s annual internet awards!
</p>
<strong>VOTE:</strong> <a href="https://noonies.tech/">noonies.tech</a> 
 
<strong>Covered in this interview: </strong>
<ul>

<li>How solve problems so relevant that your products build themselves
</li>
<li>The global shift from return on investment to return on environment
</li>
<li>Why betting on millennial interests is good business
</li>
<li>The fringe benefits of a distributed team, made up entirely of part-time freelancers and side-hustlers
</li>
<li>Why it’s important to over-emote when your team is remote
</li>
<li>Product strategy’s most transferable skills, from banking and fintech to sustainability
</li>
<strong>READ MORE ON <a href="http://hackernoon.com/">HACKERNOON.COM</a>: </strong>
</ul>

<ul>
<li><a href="https://hackernoon.com/tagged/product">https://hackernoon.com/tagged/product</a> 
</li>
<li><a href="https://hackernoon.com/tagged/sustainability/">https://hackernoon.com/tagged/sustainability/</a>
</li>
<li><a href="https://hackernoon.com/tagged/product-management/">https://hackernoon.com/tagged/product-management/</a>
</li>
<li><a href="https://hackernoon.com/tagged/product-strategy/">https://hackernoon.com/tagged/product-strategy/</a>
</li>
<li><a href="https://hackernoon.com/tagged/climate-change/">https://hackernoon.com/tagged/climate-change/</a>
</li>
</ul>
<strong>VOTE FOR YOUR BEST TECH IN THE 2020 NOONIES:</strong>

<ul>
<li><a href="https://noonies.tech/award/best-use-of-tech-for-good">https://noonies.tech/award/best-use-of-tech-for-good</a>
</li>
<li><a href="https://noonies.tech/award/best-green-tech">https://noonies.tech/award/best-green-tech</a>
</li>
<li><a href="https://noonies.tech/award/hacker-noon-contributor-of-the-year-women-in-tech">https://noonies.tech/award/hacker-noon-contributor-of-the-year-women-in-tech</a>
</li>
<li><a href="https://noonies.tech/">https://noonies.tech/</a> 
</li>
</ul>
<strong>TAKE THE AURORA SUSTAINABILITY QUIZ:</strong>

<ul>
<li><a href="https://aurorasustainability.com/quiz/">https://aurorasustainability.com/quiz/</a></li>
</ul>
<strong>JOIN THE AURORA SUSTAINABILITY COMMUNITY: </strong>
<ul>


<li><a href="https://www.instagram.com/aurorasustainability.sa/">https://www.instagram.com/aurorasustainability.sa/</a>
</li>
<li><a href="https://www.facebook.com/AuroraSustainabilityCommunity/">https://www.facebook.com/AuroraSustainabilityCommunity/</a>
</li>
</ul>