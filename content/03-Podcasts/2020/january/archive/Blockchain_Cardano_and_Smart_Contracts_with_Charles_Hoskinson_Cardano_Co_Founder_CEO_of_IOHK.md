---
id: january-17
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/blockchain-cardano-and-smart-contracts-with-charles-hoskinson-cardano-co-founder-ceo-of-iohk/">Blockchain, Cardano and Smart Contracts with Charles Hoskinson - Cardano Co-Founder, CEO of IOHK</a></h2>
<h4>January 17, 2020</h4>

<p>
<strong>Listen to the interview on <a href="https://itunes.apple.com/us/podcast/product-iteration-with-hacker-noon-interim-cto-dane-lyons/id1436233955?i=1000421970409&mt=2">iTunes</a>, or <a href="https://play.google.com/music/m/Dfuna5a4pzsmjr76bxabkxdrhim?t=Product_Iteration_with_Hacker_Noon_Interim_CTO_Dane_Lyons-Hacker_Noon_Podcast">Google Podcast</a>, or watch on <a href="https://www.youtube.com/channel/UChu5YILgrOYOfkfRlTB-D-g">YouTube</a>.</strong>
</p>
In this episode HackerHodl (Utsav) interviews Cardano co-Founder and IOHK CEO, <a href="https://iohk.io/en/team/charles-hoskinson">Charles Hoskinson</a>.
<p>
They discuss Cardano as a blockchain and an enabler of functional Smart Contracts to drive scalable, secure, and fast transactions over the chain. Cardano just launched their Shelley update that is also an incentivized testnet.
</p>
“The model for smart contracts is broken or people were sold this idea that your entire application lives on the Blockchain. The reality is it doesn't. It's a service oriented architecture. So if you're gonna have your own servers, you have clients, you have a server-client model, but there's certain parts of your business logic in your application that are too trusted. So you take them out.”
<p>
“People forget the 19th century. America had hundreds of private money. All of them failed. So then after the first generation, people said, wow, this stuff is here to stay. We can decentralize trust, create a common source of truth amongst people. We can push money around. The problem with Bitcoin is that it is blind, deaf and dumb. It doesn't understand the world around it. You can't do smart transactions.”
</p>
<em> —<strong> </strong>Charles Hoskinson</em>
 
<p>
<em>P.S. If you dig the new <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>, consider <a href="https://itunes.apple.com/us/podcast/the-hacker-noon-podcast/id1436233955?mt=2">giving us a 5 star review on iTunes.</a></em>
</p>
Also check out this month’s <a href="https://hackernoon.com/archive/2019/07">top stories</a>, <a href="https://hackernoon.com/tagged/cryptocurrency">cryptocurrency stories</a>, and <a href="http://hackernoon.com/">today’s homepage</a>.