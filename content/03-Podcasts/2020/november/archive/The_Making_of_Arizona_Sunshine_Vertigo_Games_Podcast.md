---
id: november-25
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/the-making-of-arizona-sunshine-vertigo-games-podcast/">The Making of Arizona Sunshine | Vertigo Games Podcast</a></h2>
<h4>November 25, 2020</h4>

<p>
In this podcast, Limarc talks to Arjen van Heck from Vertigo Games, one of the biggest VR game developers in the industry, and the renowned creators of the zombie apocalypse game <em>Arizona Sunshine</em>. 
</p>
 
<p>
Vertigo Games has proven to be a versatile development company, having released titles in completely different genres, such as <em>A Fisherman’s Tale</em> and <em>Skyworld: Kingdom Brawl.</em>
</p>
 
<p>
Limarc and Arjen talk about the making of <em>Arizona Sunshine</em>, porting the game over to Oculus Quest, ethical and moral decisions within VR game development, and Vertigo Games’ upcoming title, <em>After the Fall</em>. 
</p>
 
<strong>READ MORE ON HACKERNOON.COM: </strong>
<ul>

<li><a href="https://hackernoon.com/tagged/gaming">https://hackernoon.com/tagged/gaming</a>
</li>
<li><a href="https://hackernoon.com/tagged/virtual-reality">https://hackernoon.com/tagged/virtual-reality</a>
</li>
<li><a href="https://hackernoon.com/tagged/vr">https://hackernoon.com/tagged/vr</a>
</li>
</ul>
<strong>FOLLOW VERTIGO GAMES:</strong>
<ul>

<li><a href="https://vertigo-games.com/">https://vertigo-games.com/</a>
</li>
<li><a href="https://twitter.com/vertigogames">https://twitter.com/vertigogames</a>
</li>
<li><a href="https://www.youtube.com/user/VertigoGames">https://www.youtube.com/user/VertigoGames</a>
</li>
<li><a href="https://www.linkedin.com/company/vertigo-games-bv/">https://www.linkedin.com/company/vertigo-games-bv/</a>
</li>
</ul>