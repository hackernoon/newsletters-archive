---
id: february-15
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/the-definitive-bubbleball-book-and-the-future-of-the-nbas-revenue/">The Definitive Bubbleball Book and The Future of the NBA’s Revenue</a></h2>
<h4>February 15, 2021</h4>

<p>
The Washington Post's <a href="https://benjamingolliver.com/">Ben Golliver</a> joins Hacker Noon CEO <a href="https://www.davidsmooke.net/">David Smooke</a> to discuss the process of writing his new book, <a href="https://amzn.to/3b6JVTK">"Bubbleball: Inside the NBA's Fight to Save a Season,"</a> and the pandemic's impact on the NBA's revenue. With no/few fans in the stands and the decline in the number of games from the pandemic, the National Basketball Association is expected to loss about $4 billion dollars in 2021. In addition to discussing his bubble writing and publishing process, we talk about the NBA's options to stimulate revenue, such as <a href="https://www.washingtonpost.com/sports/2021/01/08/nba-expansion-seattle-las-vegas/">expansion fees for new franchises</a> and how to make substantial investments into the NBA's digital experience. 
</p>
 
<p>
Some <a href="https://hackernoon.com/">related reads on Hacker Noon</a> (where we usually publish more about things like <a href="https://hackernoon.com/tagged/software-development">software development</a>, <a href="https://hackernoon.com/tagged/blockchain">blockchains</a>, <a href="https://hackernoon.com/tagged/startup">startups</a>, and <a href="https://hackernoon.com/signup">everything tech</a>):
</p>
<ul>

<li><a href="https://podcast.hackernoon.com/e/washington-post-s-ben-golliver-discusses-nba-s-microsoft-partnership-kawhi-s-logo-lawsuit-and-the-modern-reporter-s-techstack/">NBA’s Microsoft Partnership, Kawhi’s Logo Lawsuit, and the Modern Reporter’s Techstack</a>
</li>
<li><a href="https://hackernoon.com/sports-teams-need-to-embrace-digital-innovations-to-regain-engagement-and-revenue-lost-to-covid-19-7yh310w">Sports Teams Need To Embrace Digital Innovations to Regain Engagement and Revenue Lost to COVID-19</a>
</li>
<li><a href="https://hackernoon.com/the-gaming-ecosystem-explained-nk1d32ts">The Gaming Ecosystem Explained</a>
</li>
<li><a href="https://hackernoon.com/making-up-the-best-team-in-nba-history-qfey32ei">Making Up the Best Team in NBA History</a>

<strong>Buy the book - <a href="https://amzn.to/3b6JVTK">Bubbleball: Inside the NBA's Fight to Save a Season</a></strong>
</li>
</ul>