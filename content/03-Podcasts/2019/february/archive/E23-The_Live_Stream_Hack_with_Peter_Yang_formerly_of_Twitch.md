---
id: february-5
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/the-live-stream-hack-with-peter-yang-formerly-of%c2%a0twitch/">E23 - The Live Stream Hack with Peter Yang formerly of Twitch</a></h2>
<h4>February 5, 2019</h4>

<h4><strong>Episode 23 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with <a href="https://medium.com/u/aaf44c50cd2c">Peter Yang</a>, former Product Manager at <a href="http://twitch.com/">Twitch</a>.</strong></h4>

<p>
<em>Today’s show would not be possible without <a href="https://do.co/2TTnFCL">Digital Ocean</a>. Learn more at <a href="https://do.co/2TTnFCL">do.co/hackernoon</a>.</em>
</p>
In this episode Trent Lapinski and <a href="https://medium.com/u/aaf44c50cd2c">Peter Yang</a> discuss the live streaming market including the differences between the US and Chinese markets, video game streaming, and product management.
<p>
“Live streaming is about long form content, interactive content, content you can talk to your viewers about or talk to other streamers about.”
</p>
“It is about creating these jobs for people who might not enjoy a white collar job, or driving Uber or something, who just really enjoy playing videos.”
<p>
“Why not make a career out of playing video games? Being able to connect with other people around the country playing these video games and have a thriving career that way; I think that’s just awesome.” — <a href="https://medium.com/u/aaf44c50cd2c">Peter Yang</a>
</p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>