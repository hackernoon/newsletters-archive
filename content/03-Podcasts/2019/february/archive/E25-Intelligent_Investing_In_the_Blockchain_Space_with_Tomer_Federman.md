---
id: february-11
title: ' '
---


<h2><a href="https://podcast.hackernoon.com/e/intelligent-investing-in-the-blockchain-space-with-tomer%c2%a0federman/">E25 - Intelligent Investing In the Blockchain Space with Tomer Federman</a></h2>
<h4>February 11, 2019</h4>

<h4><strong>Episode 25 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with <a href="https://medium.com/u/db459dcc870">Tomer Federman</a>, CEO and founder of <a href="https://www.federman.capital/">Federman Capital</a>.</strong></h4>

<p>
<em>Today’s show would not be possible without <a href="https://do.co/2TTnFCL">Digital Ocean</a>. Learn more at <a href="https://do.co/2TTnFCL">do.co/hackernoon</a>.</em>
</p>
In this episode Trent Lapinski and <a href="https://medium.com/u/db459dcc870">Tomer Federman</a> discuss <a href="https://medium.com/federman-capital/heres-why-i-left-the-best-job-in-the-world-to-start-an-investment-firm-focused-on-crypto-assets-9da3cce95b93">why he left Facebook</a> to start his own crypto and blockchain investment fund.
<p>
“I think this technology is truly revolutionary, is going to disrupt the financial ecosystem, and is going to have major impact across entire industries in years to come.”
</p>
“Crypto shouldn’t be about getting rich quickly. It should be about creating meaningful products that solve real world needs, and address real pain points.”
<p>
“If I’m right and blockchain is going to be so transformative, it’s obviously going to generate significant returns for investors who invest wisely. The ability for people to transact with no intermediaries involved, and for the transactions to be recorded in an open way and settled almost immediately, the opportunities for building on top of the technology is just mind blowing.” — <a href="https://medium.com/u/db459dcc870">Tomer Federman</a>
</p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>