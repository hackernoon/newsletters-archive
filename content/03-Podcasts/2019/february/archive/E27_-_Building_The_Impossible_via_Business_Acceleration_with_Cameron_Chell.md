---
id: february-26
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/building-the-impossible-via-business-acceleration-with-cameron%c2%a0chell/">E27 - Building The Impossible via Business Acceleration with Cameron Chell</a></h2>
<h4>February 26, 2019</h4>

<h4><strong>Episode 27 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with <a href="https://medium.com/u/affa0b776153">Cameron Chell</a>, Chairman and Co-Founder of <a href="https://icoxinnovations.com/">ICOx Innovations</a>, and co-creator of Kodak Coin.</strong></h4>

<p>
<em>Today’s show would not be possible without <a href="https://do.co/2TTnFCL">Digital Ocean</a>. Learn more at <a href="https://do.co/2TTnFCL">do.co/hackernoon</a>.</em> 
</p>
In this episode Trent Lapinski and <a href="https://medium.com/u/affa0b776153">Cameron Chell</a> discuss bringing blockchain technology to enterprise companies we already know and trust, and what it is going to take to move from centralized systems to decentralized systems.
<p>
“Our thinking, yet to be proven, is that we go to these established brands who are already transacting in tens or hundreds of millions of dollars in revenue and they can utilize a coin or the crypto infrastructure that can either reduce their costs or increase customer attraction.”
</p>
“Ultimately, society and certainly all economics run on trust, there is no other currency other than trust. When you have a system that can replace trust or create an environment where you can transact in a trustless way, the impact globally to me is beyond measure.” — <a href="https://medium.com/u/affa0b776153">Cameron Chell</a>
<p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
</p>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>