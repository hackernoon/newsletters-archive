---
id: february-7
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/effectively-marketing-blockchain-technology-with-erica%c2%a0blair/">E24 - Effectively Marketing Blockchain Technology with Erica Blair</a></h2>
<h4>February 7, 2019</h4>

<h4><strong>Episode 24 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with <a href="https://medium.com/u/2fb013d46fd">Erica Blair</a>, founder of <a href="http://ericablair.io/">Blockchain Branding</a>.</strong></h4>

<p>
In this episode Trent Lapinski and <a href="https://medium.com/u/2fb013d46fd">Erica Blair</a> discuss branding, messaging, and what takes to effectively market a blockchain technology company.
</p>
“I see cryptocurrencies more generally as a broad political philosophy about what it is you find important in the World.”
<p>
“Crytpocurrency is an entire governance model of saying this how decisions get made, this is how how value gets distributed.”
</p>
“We are changing the World and we’d like you to join us, oh and by the way, we’re doing it with these tools and this technology that was never available before, which is why what we're doing is so revolutionary.” — <a href="https://medium.com/u/2fb013d46fd">Erica Blair</a>
<p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
</p>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>