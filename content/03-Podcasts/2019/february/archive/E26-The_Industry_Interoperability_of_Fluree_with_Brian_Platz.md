---
id: february-20
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/the-industry-interoperability-of-fluree-with-brian%c2%a0platz/">E26 - The Industry Interoperability of Fluree with Brian Platz</a></h2>
<h4>February 20, 2019</h4>

<h4><strong>Episode 26 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with <a href="https://medium.com/u/d66c11b2e580">Brian Platz</a>, Co-CEO of <a href="http://flur.ee/">Fluree</a>.</strong></h4>

<p>
<em>Today’s show would not be possible without <a href="https://do.co/2TTnFCL">Digital Ocean</a>. Learn more at <a href="https://do.co/2TTnFCL">do.co/hackernoon</a>.</em>
</p>
In this episode Trent Lapinski and <a href="https://medium.com/u/d66c11b2e580">Brian Platz</a> discuss database blockchain technology and the variety of approaches to building decentralized applications.
<p>
“I think a lot starts changing when you can actually trust the data.”
</p>
“With Fluree, you can actually grab the exact version of the database that was being used for that integration at exactly 12:01 AM and 0.23 milliseconds, and you can actually see and know what was in there, figure out what broke, and fix it.” —<a href="https://medium.com/u/d66c11b2e580">Brian Platz</a>
<p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
</p>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>