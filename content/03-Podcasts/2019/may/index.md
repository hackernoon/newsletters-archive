---
id: may-2019
title: May 2019
sidebar_label: May
---

- [AI and Past Rediscovery with Robert Adamson](archive/may-2)
- [Smart Contracts on Blockchain with Will Martino and Tony Pham](archive/may-9)
- [A Look At the Current and Future Cryptocurrency Landscape with Writer, Noam Levenson](archive/may-14)
- [Is Decentralized Governance The Future of Tokenized Assets? - Andy Bromberg](archive/may-16)
- [Computational Journalism: The Data Behind the Stories with Jonathan Stray](archive/may-21)
- [Building Technology that Augments Human Abilities with Amber Case](archive/may-28)
- [Event Horizon: Hacker Noon 2.0 with David Smooke, Dane Lyons, and Austin Pocus](archive/may-30)