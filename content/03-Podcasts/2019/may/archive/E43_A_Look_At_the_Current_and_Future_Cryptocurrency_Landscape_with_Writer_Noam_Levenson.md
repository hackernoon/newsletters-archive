---
id: may-14
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/a-look-at-the-current-and-future-cryptocurrency-landscape-with-writer-noam-levenson/">E43 - A Look At the Current and Future Cryptocurrency Landscape with Writer, Noam Levenson</a></h2>
<h4>May 14, 2019</h4>

<p>
Episode 43 of the Hacker Noon Podcast: An interview with Writer, Noam Levenson.
</p>
 
<p>
This episode of Hacker Noon is sponsored by Indeed Prime. 
</p>
 
<p>
Visit <a href="https://www.indeedprime.com/hackernoon/">https://www.indeedprime.com/hackernoon/</a> to flip the script on the job search and join now for access to resumé reviews, 1:1 sessions with technical career coaches, personalized work-style assessments and even negotiation tips to help seal the deal.
</p>
 
<p>
Listen to the interview on iTunes, or Google Podcast, or watch on YouTube.
</p>
 
<p>
In this episode Hacker Noon Producer, <a href="http://www.haberdasherband/production">Derek Bernard</a> interviews Writer, <a href="https://hackernoon.com/@noamlevenson">Noam Levenson</a>. Topics discussed are around the current and future global effects of cryptocurrency on the economy and the world community. 
</p>
 
<p>
<em>"I think this is kind of a larger question that people sometimes approach in the wrong way"</em>
</p>
<em>“I am not sure that in these kinds of cases centralization is necessarily bad. I think the important question is not that it's decentralized or centralized. It’s that the potential for a monopoly is impossible.”</em>
<p>
<em>—<strong> </strong> Noam Levenson</em>
</p>
 
<p>
Production and music by Derek Bernard - <a href="http://haberdasherband.com/production?fbclid=IwAR2d8t0cNGHRm1ajmUNWKZ-TMUMawREhvIHSy54LKcOElf7v_TOvkAjZ78Y">haberdasherband.com/production</a>
</p>
 
<p>
<a href="https://hackernoon.com/">https://hackernoon.com/</a> 
</p>
<a href="https://community.hackernoon.com/">https://community.hackernoon.com/</a> 
<p>
<a href="https://contribute.hackernoon.com/">https://contribute.hackernoon.com/</a> 
</p>
<a href="https://sponsor.hackernoon.com/">https://sponsor.hackernoon.com/</a>  
<p>
<a href="https://podcast.hackernoon.com/">https://podcast.hackernoon.com/</a> 
</p>
<a href="https://twitter.com/hackernoon/">https://twitter.com/hackernoon/</a> 
<a href="https://facebook.com/hackernoon/">https://facebook.com/hackernoon/</a>