---
id: may-9
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/smart-contracts-on-blockchain-with-will-martino-and-tony-pham/">E42 - Smart Contracts on Blockchain with Will Martino and Tony Pham</a></h2>
<h4>May 9, 2019</h4>

<p>
Episode 42 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with Will Martino, Founder & CEO at <a href="https://kadena.io/en/">Kadena</a> and Tony Pham, Head of Marketing at <a href="https://kadena.io/en/">Kadena</a>.
</p>
This episode of Hacker Noon is sponsored by DigitalOcean. Discover why developers love DigitalOcean and get started with a free $100 credit at https://do.co/hackernoon
<h4><strong>Listen to the interview on <a href="https://itunes.apple.com/us/podcast/product-iteration-with-hacker-noon-interim-cto-dane-lyons/id1436233955?i=1000421970409&mt=2">iTunes</a>, or <a href="https://play.google.com/music/m/Dfuna5a4pzsmjr76bxabkxdrhim?t=Product_Iteration_with_Hacker_Noon_Interim_CTO_Dane_Lyons-Hacker_Noon_Podcast">Google Podcast</a>, or watch on <a href="https://www.youtube.com/channel/UChu5YILgrOYOfkfRlTB-D-g">YouTube</a>.</strong></h4>


<p>
In this episode Trent Lapinski interviews Will Martino, founder and CEO at <a href="https://kadena.io/en/">Kadena</a>, and Tony Pham, Head of Marketing . You get to discover what they’re working on, which is a new programming language for smart contracting and scalability and use cases for enterprise blockchain solutions.
</p>
<p>
<em>"It's all about aligning the incentives of the three core users of the platform. You have miners who want more adoption, they want a bigger network doing more transactions a second. You have users who don't want to pay high fees. They want high security, they want decentralization, they want trustlessness, they want openness, and they also don't want to pay fees. And businesses who want to know that if they start using this platform and they start congesting the network, because think Cryptokitties - it launches and Ethereum gets clogged for a week, they want to know that the network can grow to support the throughput they need because they found their product-market fit."</em>
</p>
 
<p>
<em>"Overall, where I see the whole space going, is the sharing economy - the Enterprise sharing economy. This is one where you're redefining how consumers and businesses interact."</em>
</p>
<p>
<em>—<strong> </strong> Will Martino</em>
</p>
 
<p>
Production and music by Derek Bernard - <a href="http://haberdasherband.com/production?fbclid=IwAR2d8t0cNGHRm1ajmUNWKZ-TMUMawREhvIHSy54LKcOElf7v_TOvkAjZ78Y">haberdasherband.com/production</a>
</p>
Host: Trent Lapinski - <a href="https://trentlapinski.com/">https://trentlapinski.com</a>
 
<p>
<a href="https://hackernoon.com/">https://hackernoon.com/</a>  
</p>
<a href="https://community.hackernoon.com/">https://community.hackernoon.com/</a> 
<p>
<a href="https://contribute.hackernoon.com/">https://contribute.hackernoon.com/</a> 
</p>
<a href="https://sponsor.hackernoon.com/">https://sponsor.hackernoon.com/</a> 
<p>
<a href="https://podcast.hackernoon.com/">https://podcast.hackernoon.com/</a> 
</p>
<a href="https://twitter.com/hackernoon/">https://twitter.com/hackernoon/</a> 
<a href="https://facebook.com/hackernoon/">https://facebook.com/hackernoon/</a> 