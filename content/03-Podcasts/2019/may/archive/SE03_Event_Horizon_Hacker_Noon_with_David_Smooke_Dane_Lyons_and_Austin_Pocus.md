---
id: may-30
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/se03-the-event-horizon-hacker-noon-20-with-david-smooke-dany-lyons-and-austin-pocus/">SE03 - Event Horizon: Hacker Noon 2.0 with David Smooke, Dane Lyons, and Austin Pocus</a></h2>
<h4>May 30, 2019</h4>

<p>
This episode of Hacker Noon is sponsored by DigitalOcean. Discover why developers love DigitalOcean and get started with a free $50 credit at <a href="https://do.co/hackernoon">https://do.co/hackernoon</a>
</p>
 
<p>
Today we bring you special episode from London, England. 
</p>
 
<p>
Hacker Noon CEO David Smooke has a conversation with Hacker Noon's CPO Dane Lyons and Full Stack Developer Austin Pocus. 
</p>
 
<p>
They discuss the current state of progress of Hacker Noon 2.0, the tribulations of the process, and what they'd like to see in the final product.
</p>
 
<p>
It's a great episode for those eagerly anticipating the next iteration of Hacker Noon.
</p>
 
<p>
Stick around!
</p>
Production and music by Derek Bernard - <a href="http://www.haberdasherband.com/production">www.haberdasherband.com/production</a>
<p>
<a href="https://hackernoon.com/">https://hackernoon.com/</a>
</p>
<a href="https://community.hackernoon.com/">https://community.hackernoon.com/</a>
<p>
<a href="https://contribute.hackernoon.com/">https://contribute.hackernoon.com/</a>
</p>
<a href="https://sponsor.hackernoon.com/">https://sponsor.hackernoon.com/</a>
<p>
<a href="https://podcast.hackernoon.com/">https://podcast.hackernoon.com/</a>
</p>
<a href="https://twitter.com/hackernoon/">https://twitter.com/hackernoon/</a>
<p>
<a href="https://facebook.com/hackernoon/">https://facebook.com/hackernoon/</a>
</p>