---
id: may-21
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/computational-journalism-the-data-behind-the-stories-with-jonathan-stray/">E45 - Computational Journalism: The Data Behind the Stories with Jonathan Stray</a></h2>
<h4>May 21, 2019</h4>

<h3>Episode 45 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with <a href="http://jonathanstray.com/me">Jonathan Stray</a>, computational journalist and researcher at Columbia Journalism School.</h3>

<p>
This episode of Hacker Noon is sponsored by Indeed Prime.
</p>
<p>
Visit <a href="https://www.indeedprime.com/hackernoon/">https://www.indeedprime.com/hackernoon/</a> to flip the script on the job search and join now for access to resumé reviews, 1:1 sessions with technical career coaches, personalized work-style assessments and even negotiation tips to help seal the deal.
</p>
<p>
<strong>Listen to the interview on <a href="https://podcasts.apple.com/us/podcast/hacker-noon-podcast/id1436233955">iTunes </a>or watch on <a href="https://youtu.be/vYCfv0-yUU4">YouTube</a>.</strong>
</p>
<p>
In this episode Cris Beasley interviews <a href="http://jonathanstray.com/me">Jonathan Stray</a>, computational journalist, data scientist, researcher at Columbia Journalism School and a fellow at the Institute for The Future.
</p>
<p>
You get to meet the creator of <a href="http://workbenchdata.com/">Workbench</a>, the tool for analyzing big data without any coding experience necessary.
</p>
 
<p>
"I identify as a computational journalist and I tell people it's two things. It's using computational techniques to do stories and doing stories about how computation and algorithms are affecting Society.”
</p>
 
<p>
“One direction might be using data science for investigative journalism. And the other direction might be, for example, looking into how machine learning is being used to decide who has to stay in jail before their trial.”
</p>
 
<p>
<em> —</em> <em>Jonathan Stray</em>
</p>
 
<p>
Production and music by Derek Bernard - <a href="http://haberdasherband.com/production?fbclid=IwAR2d8t0cNGHRm1ajmUNWKZ-TMUMawREhvIHSy54LKcOElf7v_TOvkAjZ78Y">haberdasherband.com/production</a>
</p>
Host: Cris Beasley - <a href="https://hackernoon.com/@crisbeasley">https://hackernoon.com/@crisbeasley</a>
<em>P.S. If you dig the new <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>, consider <a href="https://itunes.apple.com/us/podcast/the-hacker-noon-podcast/id1436233955?mt=2">giving us a 5 star review on iTunes.</a></em>
<h4>Also check out <a href="https://hackernoon.com/archive/2019/05">Mays top stories</a>,<strong> </strong><a href="https://hackernoon.com/latest-tech-stories/home">the latest stories</a><strong>, </strong>and <a href="http://hackernoon.com/">today’s homepage</a>.</h4>