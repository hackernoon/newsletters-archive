---
id: may-2
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/ai-and-past-rediscovery-with-robert-adamson/">E41 - AI and Past Rediscovery with Robert Adamson</a></h2>
<h4>May 2, 2019</h4>

<h2>Episode 41 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with Robert Adamson, a computer programmer and science fiction writer.</h2>

<h4><strong><em>This episode of Hacker Noon is sponsored by DigitalOcean. Discover why developers love DigitalOcean and get started with a free $100 credit at <a href="https://l.facebook.com/l.php?u=https%3A%2F%2Fdo.co%2Fhackernoon%3Ffbclid%3DIwAR0rlAqFCLte3_f1pIJLMWlLOZPbjRu6CooU-FoR4-h6eoupceNQJIMLDag&h=AT3ylLdnTJBMQGXK51tG8ipTxCjNE8is7-ZgdJ25PrSH5oXj4ATRlNo0wRenVRK9yyYf4_Eh4Z4-wqOy7TOzNCBd5xx4q7FSehUujYFwevF533P7Xwb_Cmw-E9wIx6e4oobttqfALqMTo_55Bsn8GY-6ds-LVZzSG0bpXfgKAFga6i8K86eEISam5fUji5qhTxqC-ePiDuqaXMT6Q92EXwzvT-EBG3KFdIkKLxId-3heeyKmBpbh2iCkq68omoYnIoMjznPtMz9sqsSKvVpP_sf7bTVSx4oVuCuTSBHH52jRr1yU3VqQjEaX7ZKbqxPVGPdT4oXEwEmZx40p1bPm8BXMem2y2A6p7LvhDkuzeFJ_tmllLDI6BTYU0xPwCIpkQOwxWkqQHk5W9UEBGS6RxNqtTpL3x0utD_oSThsDbdGnB5RQ82SIlC2TRqjSaqP7dV63cwcvhVH4-Ry8_uBUcSvhSz3oJW1pNK5E8VXn8GEfWUrZ6On8JJLPeQhg8eGdhJCUFVRJBuA_x5fgTOTdpAcTz_88ryuFRa5bwG2SbdHRD484N2reIGiEdcU5h8MVQ0Gou_OkG-XTzjIcbl23VNjbeL8lE4GUILSqJr68njEGdGxEUwOLeCQ75aWF_gT6SyxD_Vx6KEo8CluKx88oJVHg">https://do.co/hackernoon</a></em></strong></h4>

<h4><strong>Listen to the interview on <a href="https://itunes.apple.com/us/podcast/product-iteration-with-hacker-noon-interim-cto-dane-lyons/id1436233955?i=1000421970409&mt=2">iTunes</a>, or <a href="https://play.google.com/music/m/Dfuna5a4pzsmjr76bxabkxdrhim?t=Product_Iteration_with_Hacker_Noon_Interim_CTO_Dane_Lyons-Hacker_Noon_Podcast">Google Podcast</a>, or watch on <a href="https://www.youtube.com/channel/UChu5YILgrOYOfkfRlTB-D-g">YouTube</a>.</strong></h4>

<p>
In this episode Trent Lapinski interviews Robert Adamson, you get to discover Rob’s recent app which helps you to identify aliens and learn about ancient civilizations and how we can use AI and machine learning to rediscover our past.
</p>
<p>
“Something is not real, something about this whole reality thing is not right. And so simulated reality is a theory that we are living in the computer simulation.”
</p>
“The Earth as we know it is really simulated, some really advanced civilizations in the future could not computered in.”
<p>
“The virtual reality... we go there for whatever reason maybe to learn, to get education whatever. That is the theory of simulated reality. And it seems to answer all kinds of questions. Because you are thinking universe is data as opposed to matter in time and space.”<em>—<strong> </strong> Robert Adamson</em>
</p>
<p>
Production and music by Derek Bernard - <a href="http://haberdasherband.com/production?fbclid=IwAR2d8t0cNGHRm1ajmUNWKZ-TMUMawREhvIHSy54LKcOElf7v_TOvkAjZ78Y">haberdasherband.com/production</a>
</p>
Host: Trent Lapinski - <a href="https://trentlapinski.com/">https://trentlapinski.com </a>
<p>
<a href="https://hackernoon.com/">https://hackernoon.com/</a>
</p>
<a href="https://community.hackernoon.com/">https://community.hackernoon.com/</a> 
<p>
<a href="https://contribute.hackernoon.com/">https://contribute.hackernoon.com/</a> 
</p>
<a href="https://sponsor.hackernoon.com/">https://sponsor.hackernoon.com/</a> 
<p>
<a href="https://podcast.hackernoon.com/">https://podcast.hackernoon.com/</a> 
</p>
<a href="https://twitter.com/hackernoon/">https://twitter.com/hackernoon/</a> 
<p>
<a href="https://facebook.com/hackernoon/">https://facebook.com/hackernoon/</a> 
</p>