---
id: july-2019
title: July 2019
sidebar_label: July
---

- [#CrazyTechStories @ Huckletree, London, England](archive/july-2)
- [The Shortest Distance From Idea to Execution with Jim Rose of CircleCI](archive/july-11)
- [Centralized, Decentralized, and Independent Media with David Smooke of Hacker Noon](archive/july-17)
- [IBM’s Four Pillars for Enterprise Blockchain Development with Jerry Cuomo of IBM](archive/july-18)
- [#CrazyTechStories @ GitHub, San Francisco, CA, United States](archive/july-23)
- [From Liberal Arts to Tech with Lauren Maffeo](archive/july-30)