---
id: july-11
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/e55-jim-rose/">E56 - The Shortest Distance From Idea to Execution with Jim Rose of CircleCI</a></h2>
<h4>July 11, 2019</h4>

<p>Episode 56 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with <a href="https://twitter.com/jimdotrose">Jim Rose</a>, CEO of <a href="https://circleci.com/">CircleCI</a>, platform for software teams which allows to rapidly build quality projects, at scale. CircleCI’s mission is to give people everywhere the power to build and deliver software at the speed of imagination.</p>


<p>
This episode of Hacker Noon is sponsored by DigitalOcean. Discover why developers love DigitalOcean and get started with a free $50 credit at <a href="https://do.co/hackernoon">https://do.co/hackernoon</a>
<h4><strong>Listen to the interview on <a href="https://podcasts.apple.com/us/podcast/e56-shortest-distance-from-idea-to-execution-jim-rose/id1436233955?i=1000444246805">iTunes</a> or watch on <a href="https://youtu.be/PJ9S_-gu2Uo">YouTube</a>.</strong></h4>
</p>

<p>
In this episode Trent Lapinski interviews <a href="https://twitter.com/jimdotrose">Jim Rose </a> from <a href="https://circleci.com/">CircleCI</a>.  You get to discover the insights of continuous delivery and automation processes and learn what it takes to automate your apps in the cloud. 
</p>
<em>“Our model has always been about developer empowerment. We've always wanted developers to use our platform irrespective of whether they're going to pay us, because we think, in a very community-oriented fashion, that a rising tide raises all the ships. The more developers can get familiar with more advanced technologies and more advanced approaches it's just good for everyone, including their consumers” —<strong> </strong> Jim Rose</em>
<p>
Production and music by Derek Bernard - <a href="http://www.haberdasherband.com/production">www.haberdasherband.com/production</a>
</p>
<p>
Host: Trent Lapinski - <a href="https://trentlapinski.com/">https://trentlapinski.com</a>
</p>
 
<p>
<a href="https://hackernoon.com/">https://hackernoon.com/</a> 
</p>
<a href="https://community.hackernoon.com/">https://community.hackernoon.com/</a> 
<p>
<a href="https://contribute.hackernoon.com/">https://contribute.hackernoon.com/</a>  
</p>
<a href="https://sponsor.hackernoon.com/">https://sponsor.hackernoon.com/</a> 
<p>
<a href="https://podcast.hackernoon.com/">https://podcast.hackernoon.com/</a>  
</p>
<a href="https://twitter.com/hackernoon/">https://twitter.com/hackernoon/</a> 
<p>
<a href="https://facebook.com/hackernoon/">https://facebook.com/hackernoon/</a>
</p>
 
<em>P.S. If you dig the new <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>, consider <a href="https://itunes.apple.com/us/podcast/the-hacker-noon-podcast/id1436233955?mt=2">giving us a 5 star review on iTunes.</a></em>
<h4>Also check out the <a href="https://hackernoon.com/archive/2019/07">top stories from July</a>,<strong> </strong><a href="https://hackernoon.com/latest-tech-stories/home">the latest stories</a>, and <a href="http://hackernoon.com/">today’s homepage</a>.</h4>