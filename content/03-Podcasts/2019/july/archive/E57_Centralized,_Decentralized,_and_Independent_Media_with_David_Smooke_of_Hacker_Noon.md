---
id: july-17
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/centralized-decentralized-and-independent-media-with-david-smooke-of-hacker-noon/">E57 - Centralized, Decentralized, and Independent Media with David Smooke of Hacker Noon</a></h2>
<h4>July 17, 2019</h4>

<p>Episode 57 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with David Smooke of <a href="https://hackernoon.com/">Hacker Noon</a></p>


<p>
Sponsor:<a href="https://alpaca.markets/">Alpaca - “Hack Financial Systems”</a>
<h4><strong>Listen to the interview on <a href="https://itunes.apple.com/us/podcast/product-iteration-with-hacker-noon-interim-cto-dane-lyons/id1436233955?i=1000421970409&mt=2">iTunes</a>, or <a href="https://play.google.com/music/m/Dfuna5a4pzsmjr76bxabkxdrhim?t=Product_Iteration_with_Hacker_Noon_Interim_CTO_Dane_Lyons-Hacker_Noon_Podcast">Google Podcast</a>, or watch on <a href="https://www.youtube.com/channel/UChu5YILgrOYOfkfRlTB-D-g">YouTube</a>.</strong></h4>
</p>
 
<p>
In this episode <a href="http://www.cazzellreport.com/podcast">Amber Cazzell</a> interviews David Smooke of Hacker Noon.  David shares his insights and experiences taking a startup from conception to successful business in the digital publication space. As well as the decisions that motivated the move from Medium to developing Hacker Noon’s own platform.
</p>
 
<p>
"The writing is on the wall that the next step for Hacker Noon is that it needs to be its own software.<em>”</em>
</p>
<em>“</em>People telling their own stories. We want to level that up by like 100x, and, the rich people deciding what stories should trend, try and slow that down.<em>” </em>
<p>
<em>“</em>Where we’re at in the landscape is we’re not trying to break news. We’re trying to get experts to amplify their voice. So when people are doing cool things in technology - research papers from Stanford for example - like that’s an interesting thing for us to republish and get very smart people who are building related things to read.<em>”</em>
</p>
<em>“</em>I think that young entrepreneurs are better off publishing something small each day than trying to spend two weeks getting into BBC or CNN or whoever their target is. How do I reduce the barriers for me to get something out each day? And each day I can share more of my story and my ideas, and that will attract more people that are interested in those ideas too.<em>”  —<strong> </strong>David Smooke</em>
<p>
Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a>
</p>
Host Amber Cazzell - <a href="https://www.cazzellreport.com/">https://www.cazzellreport.com/</a>
<p>
<a href="https://hackernoon.com/">https://hackernoon.com/</a> 
</p>
<a href="https://community.hackernoon.com/">https://community.hackernoon.com/</a> 
<p>
<a href="https://contribute.hackernoon.com/">https://contribute.hackernoon.com/</a> 
</p>
<a href="https://sponsor.hackernoon.com/">https://sponsor.hackernoon.com/</a> 
<p>
<a href="https://podcast.hackernoon.com/">https://podcast.hackernoon.com/</a> 
</p>
<a href="https://twitter.com/hackernoon/">https://twitter.com/hackernoon/</a> 
<p>
<a href="https://facebook.com/hackernoon/">https://facebook.com/hackernoon/</a> 
</p>
<em>P.S. If you dig the new <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>, consider <a href="https://itunes.apple.com/us/podcast/the-hacker-noon-podcast/id1436233955?mt=2">giving us a 5 star review on iTunes.</a></em>
<h4>Also check out the <a href="https://hackernoon.com/archive/2019/07">top stories from July</a>,<strong> </strong><a href="https://hackernoon.com/latest-tech-stories/home">the latest stories</a>, and <a href="http://hackernoon.com/">today’s homepage</a>.</h4>