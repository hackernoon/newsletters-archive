---
id: july-18
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/ibms-4-things-necessary-for-effective-use-of-blockchain/">E58 - IBM’s Four Pillars for Enterprise Blockchain Development with Jerry Cuomo of IBM</a></h2>
<h4>July 18, 2019</h4>

<p>Episode 58 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with Jerry Cuomo of IBM</p>


<p>This episode of Hacker Noon is sponsored by DigitalOcean. Discover why developers love DigitalOcean and get started with a free $50 credit at <a href="https://do.co/hackernoon">https://do.co/hackernoon</a> </p>


<h4><strong>Listen to the interview on <a href="https://itunes.apple.com/us/podcast/product-iteration-with-hacker-noon-interim-cto-dane-lyons/id1436233955?i=1000421970409&mt=2">iTunes</a>, or <a href="https://play.google.com/music/m/Dfuna5a4pzsmjr76bxabkxdrhim?t=Product_Iteration_with_Hacker_Noon_Interim_CTO_Dane_Lyons-Hacker_Noon_Podcast">Google Podcast</a>, or watch on <a href="https://www.youtube.com/channel/UChu5YILgrOYOfkfRlTB-D-g">YouTube</a>.</strong></h4>
 
<p>
In this episode Trent Lapinski interviews Jerry Cuomo on IBM’s open-source initiatives towards enterprise Blockchain technologies, going into the value-add of Hyperledger and Hyperledger Fabric to enterprise operations.
</p>
 
<p>
"These are problems that are not waiting for tomorrow to be solved.<em>”</em>
</p>
<em>“</em>There is a wonderful moment when a developer meets a business-informed person, and there is this cross-pollination that occurs where an age-old business problem. with Blockchain now as a set of tools and techniques for businesses to collaborate, provides a resolution.<em>” </em>
<p>
<em>“</em>When you see the sparkle in that founder’s eye. That, you know, ‘this has been a thorn in our side for generations’, and now we recognize it as an industry.<em>” </em>
</p>
<em>—<strong> </strong>Jerry Cuomo</em>
<p>
Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a>
</p>
<a href="https://hackernoon.com/">https://hackernoon.com/</a> 
<p>
<a href="https://community.hackernoon.com/">https://community.hackernoon.com/</a> 
</p>
<a href="https://contribute.hackernoon.com/">https://contribute.hackernoon.com/</a> 
<p>
<a href="https://sponsor.hackernoon.com/">https://sponsor.hackernoon.com/</a> 
</p>
<a href="https://podcast.hackernoon.com/">https://podcast.hackernoon.com/</a> 
<p>
<a href="https://twitter.com/hackernoon/">https://twitter.com/hackernoon/</a> 
</p>
<a href="https://facebook.com/hackernoon/">https://facebook.com/hackernoon/</a> 
<em>P.S. If you dig the new <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>, consider <a href="https://itunes.apple.com/us/podcast/the-hacker-noon-podcast/id1436233955?mt=2">giving us a 5 star review on iTunes.</a></em>
<h4>Also check out the <a href="https://hackernoon.com/archive/2019/07">top stories from July</a>,<strong> </strong><a href="https://hackernoon.com/latest-tech-stories/home">the latest stories</a>, and <a href="http://hackernoon.com/">today’s homepage</a></h4>