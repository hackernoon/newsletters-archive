---
id: july-30
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/e59-breaking-into-tech-with-a-background-in-liberal-arts-with-lauren-maffeo/">E59 - From Liberal Arts to Tech with Lauren Maffeo</a></h2>
<h4>July 30, 2019</h4>

<p>Episode 59 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with <a href="https://hackernoon.com/@lauren-maffeo">Lauren Maffeo</a> of <a href="https://www.getapp.com/">GetApp</a></p>


<h4><strong>Listen to the interview on <a href="https://itunes.apple.com/us/podcast/product-iteration-with-hacker-noon-interim-cto-dane-lyons/id1436233955?i=1000421970409&mt=2">iTunes</a>, or <a href="https://play.google.com/music/m/Dfuna5a4pzsmjr76bxabkxdrhim?t=Product_Iteration_with_Hacker_Noon_Interim_CTO_Dane_Lyons-Hacker_Noon_Podcast">Google Podcast</a>, or watch on <a href="https://www.youtube.com/channel/UChu5YILgrOYOfkfRlTB-D-g">YouTube</a>.</strong></h4>


<p>
In this episode <a href="http://www.haberdasherband.com/">Derek Bernard</a> interviews Lauren Maffeo of GetApp.  Lauren shares how she went from a Media Studies degree in Liberal Arts Media to Research Analyst of Technology at GetApp. 
</p>
“I majored in media studies in college pretty intent on going into journalism when I graduated. Unfortunately about halfway through college the recession hit and that was about the same time that ad spend was shifting from news organizations and news sites over to digital websites like Facebook and Google who now own an enormous total collective ad spend”
<p>
“The business model for a lot of journalism outlets collapsed and what was a competitive industry before became very difficult to enter after the fact.”
</p>
"In hindsight I think my media studies degree I thought at the time that those degrees were going to prepare me for a career as a reporter, but I actually think they were better preparation for what I do now as a research analyst because that kind of education primes you to look at a market, find gaps within that market, ask critical questions about the status quo and then give solutions for what to do differently. And I see a lot of parallels between my work as an analyst here and what I did academically, and so in hindsight that humanities education was great preparation for a career in tech.<em>”</em>
<p>
<em> —<strong> </strong>Lauren Maffeo</em>
</p>
Host, production, and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a>
<p>
<a href="https://hackernoon.com/">https://hackernoon.com/</a> 
</p>
<a href="https://community.hackernoon.com/">https://community.hackernoon.com/</a> 
<p>
<a href="https://contribute.hackernoon.com/">https://contribute.hackernoon.com/</a> 
</p>
<a href="https://sponsor.hackernoon.com/">https://sponsor.hackernoon.com/</a> 
<p>
<a href="https://podcast.hackernoon.com/">https://podcast.hackernoon.com/</a> 
</p>
<a href="https://twitter.com/hackernoon/">https://twitter.com/hackernoon/</a> 
<p>
<a href="https://facebook.com/hackernoon/">https://facebook.com/hackernoon/</a> 
</p>
<em>P.S. If you dig the new <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>, consider <a href="https://itunes.apple.com/us/podcast/the-hacker-noon-podcast/id1436233955?mt=2">giving us a 5 star review on iTunes.</a></em>
<h4>Also check out the <a href="https://hackernoon.com/archive/2019/07">top stories from July</a>,<strong> </strong><a href="https://hackernoon.com/latest-tech-stories/home">the latest stories</a>, and <a href="http://hackernoon.com/">today’s homepage</a>.</h4>