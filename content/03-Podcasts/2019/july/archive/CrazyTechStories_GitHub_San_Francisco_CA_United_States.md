---
id: july-23
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/crazytechstories-github-san-francisco-ca-united-states/">#CrazyTechStories @ GitHub, San Francisco, CA, United States</a></h2>
<h4>July 23, 2019</h4>

<p>
Where Hacker Noon brings you a handful of stories from the community at large.
</p>
<p>This #CrazyTechStories is part of a larger series of talks presented in association with PubNub and GitHub at GitHub's San Francisco location.</p>

<p>
This episode of Hacker Noon is sponsored by PubNub. 
</p>
Visit <a href="https://www.pubnub.com/">https://www.pubnub.com</a> to check out how PubNub brings you APIs for realtime apps
<p>In this episode you'll hear three talks from three Hacker Noon contributing writers:</p>


<p>
<strong>Pranava Adduri</strong> - <a href="https://www.youtube.com/watch?v=TSRwI1x3cW0&list=PLzeK5ofgBRBHuVA3Y8V3U_dvL6uVfE0aG&index=6">Productivity Hacker Using Modern Text Editors</a>
</p>
<strong>Lily Chen</strong> - <a href="https://www.youtube.com/watch?v=0zesFi4Zxm4&list=PLzeK5ofgBRBHuVA3Y8V3U_dvL6uVfE0aG&index=5">Feature Fatigue Kills UX</a>
<p>
<strong>Jordan Schuetz</strong> - <a href="https://www.youtube.com/watch?v=TcdVif7ay84&list=PLzeK5ofgBRBHuVA3Y8V3U_dvL6uVfE0aG&index=7">Real Time APIs that Transform the Human Experience (feat. Peloton & Logitech)</a>
</p>
 
<h4><strong>Listen to the talks on <a href="https://podcasts.apple.com/us/podcast/crazytechstories-huckletree-london-england/id1436233955?i=1000443502826">iTunes</a> or watch on <a href="https://www.youtube.com/channel/UChu5YILgrOYOfkfRlTB-D-g">YouTube</a>.</strong></h4>


<p>
Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a>
</p>
<a href="https://hackernoon.com/">https://hackernoon.com/</a> 
<p>
<a href="https://community.hackernoon.com/">https://community.hackernoon.com/</a>  
</p>
<a href="https://contribute.hackernoon.com/">https://contribute.hackernoon.com/</a> 
<p>
<a href="https://sponsor.hackernoon.com/">https://sponsor.hackernoon.com/</a> 
</p>
<a href="https://podcast.hackernoon.com/">https://podcast.hackernoon.com/</a> 
<p>
<a href="https://twitter.com/hackernoon/">https://twitter.com/hackernoon/</a> 
</p>
<a href="https://facebook.com/hackernoon/">https://facebook.com/hackernoon/</a> 
<em>P.S. If you dig the new <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>, consider <a href="https://itunes.apple.com/us/podcast/the-hacker-noon-podcast/id1436233955?mt=2">giving us a 5 star review on iTunes.</a></em>
<h4>Also check out the <a href="https://hackernoon.com/archive/2019/07">top stories from July</a>,<strong> </strong><a href="https://hackernoon.com/latest-tech-stories/home">the latest stories</a>, and <a href="http://hackernoon.com/">today’s homepage</a>.</h4>