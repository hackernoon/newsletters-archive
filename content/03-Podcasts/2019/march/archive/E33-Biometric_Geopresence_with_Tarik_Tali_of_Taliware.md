---
id: march-28
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/biometric-geopresence-with-tarik-tali-of%c2%a0taliware/">E33 - Biometric Geopresence with Tarik Tali of Taliware</a></h2>
<h4>March 28, 2019</h4>

<h4><strong>Episode 33 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with Tarik Tali, the CEO and founder of <a href="http://taliware.com/">Taliware</a>.</strong></h4>

<p>
In this episode Trent Lapinski and Tarik Tali meetup at <a href="https://www.startupgrind.com/">Startup Grind</a> and talk about his new biometric geopresence app now available on the App Store, and the implications of this emerging technology. <em>Disclosure: Trent is an advisor on this project.</em>
</p>
“We do geolocation, geopresence verification, we tether the phone to the owner, and the location where they are.”
<p>
“What we did, our system actually has a calendar, and it tracks every day separately and stores it in the cloud. All your checkins, if you go to Starbucks and purchase coffee, you checkin, and you use your Touch or FaceID and that record is kept.” — Tarik Tali
</p>
<em>Production and music by Derek Bernard —<a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
<em>Host: Trent Lapinski —  <a href="https://trentlapinski.com/">https://trentlapinski.com</a></em>