---
id: march-12
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/the-future-of-robotics-development-with-eva-li-of-vincross/">E29 - The Future of Robotics Development with Eva Li of Vincross</a></h2>
<h4>March 12, 2019</h4>

<p>
<strong>Episode 29 of the Hacker Noon Podcast: An interview with Eva Li of Vincross, a consumer robotics company. Check it out here: <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.vincross.com%2F&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://www.vincross.com/</a></strong>
</p>
 
<p>
In this episode Trent Lapinski interviews Eva Li from Vincross, you get to meet one of the robots Hexa and learn about their new robotics development platform.
</p>
 
<p>
"HEXA is a six-legged programmable robot.”
</p>
 
<p>
“Our users are, but not limited to, software developers, programmers, researchers, people who are into robotics, hobbyists, and some students who are learning coding through HEXA.”
</p>
 
<p>
“You can not just learn programming, learn robotics, also you can make it actually useful in your daily life. We offer this ideal hardware and software to make your ideas come true” — Eva Li
</p>
 
<p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
</p>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>