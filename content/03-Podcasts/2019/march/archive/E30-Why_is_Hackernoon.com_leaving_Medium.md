---
id: march-15
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/why-is-hackernooncom-leaving%c2%a0medium/">E30 - Why is Hackernoon.com leaving Medium?</a></h2>
<h4>March 15, 2019</h4>

<h4><strong>Episode 30 of the Hacker Noon Podcast: An interview with David Smooke, CEO and founder of <a href="https://hackernoon.com/">https://Hackernoon.com</a>.</strong></h4>

<p>
In this episode Trent Lapinski interviews David Smooke concerning the future of Hackernoon.com. They discuss some of the recent events concerning Medium, and why Hackernoon.com will eventually be moving away from Medium with the coming launch of Hacker Noon 2.0.
</p>
<em>Music by Derek Bernard — <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
<em>Host: Trent Lapinski —  <a href="https://trentlapinski.com/">https://trentlapinski.com</a></em>