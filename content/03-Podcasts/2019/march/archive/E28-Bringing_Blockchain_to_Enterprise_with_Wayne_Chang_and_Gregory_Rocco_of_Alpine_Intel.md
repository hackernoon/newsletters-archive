---
id: march-5
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/bringing-blockchain-to-enterprise-with-wayne-chang-and-gregory-rocco-of-alpine%c2%a0intel/">E28 - Bringing Blockchain to Enterprise with Wayne Chang and Gregory Rocco of Alpine Intel</a></h2>
<h4>March 5, 2019</h4>

<h4><strong>Episode 28 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with Wayne Chang and Gregory Rocco of <a href="https://www.alpineintel.com/">Alpine Intel</a>, a new unit within <a href="https://consensys.net/">ConsenSys</a>.</strong></h4>

<p>
In this episode Trent Lapinski interviews Wayne Chang and Gregory Rocco from Alpine Intel to discuss bringing blockchain solutions to enterprise companies.
</p>
“Alpine is a new unit within ConsenSys. We were effectively the cryptoeconomics team at Token Foundry and we found a lot of demand for our services, and basically we want to build these new economies.”—Wayne Chang
<p>
“What we look to do is engineer value rather than create speculative value. We’re personally not affected by crypto winter. As Wayne mentioned, we’re focused a lot on enterprise too and seeing where the value can be driven in enterprises.” — Gregory Rocco
</p>
“The combination of a global computer, smart contracts, tokens, all these things are going to help drive the transaction cost down of doing business, and you’re going to unlock new kinds of transactions that are going to form new markets that we couldn’t even imagine before.” — Wayne Chang
<p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
</p>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>