---
id: march-19
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/speaking-of-crypto-with-shannon%c2%a0grinnell/">E31 - Speaking Of Crypto with Shannon Grinnell</a></h2>
<h4>March 19, 2019</h4>

<h4><strong>Episode 31 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview Shannon Grinnell, host of <a href="https://speakingofcrypto.com/">Speaking of Crypto Podcast</a>.</strong></h4>

<p>
In this episode Trent Lapinski interviews Shannon Grinnell where they discuss blockchain, cryptocurrencies, and Shannon’s background. You can also check out <a href="https://speakingofcrypto.com/054-trent-lapinski/">Trent being interviewed by Shannon on her podcast</a>, where they discuss crypto visionaries, tech for Trump, and why Apple bites.
</p>
<em>“I thought I would do a podcast on Bitcoin or cryptocurrencies but it ended up being more on blockchain technologies because the people that I met were really focused on building something with this really incredible technology.” — Shannon </em>Grinnell
<p>
<em>Music by Derek Bernard — <a href="http://haberdasherband.com/production?fbclid=IwAR2d8t0cNGHRm1ajmUNWKZ-TMUMawREhvIHSy54LKcOElf7v_TOvkAjZ78Y">haberdasherband.com/production</a></em>
</p>
<em>Host: Trent Lapinski — <a href="https://trentlapinski.com/">https://trentlapinski.com</a></em>