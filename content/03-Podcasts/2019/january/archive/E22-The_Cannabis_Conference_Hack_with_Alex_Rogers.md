---
id: january-29
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/the-cannabis-conference-hack-with-alex%c2%a0rogers/">E22 - The Cannabis Conference Hack with Alex Rogers</a></h2>
<h4>January 29, 2019</h4>

<h4><strong>Episode 22 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with Alex Rogers, CEO of <a href="https://internationalcbc.com/">ICBC the International Cannabis Business Conference</a>.</strong></h4>

<p>
In this episode Trent Lapinski and Alex Rogers discuss the cannabis industry, legalization, regulations, and what is happening internationally around the World as cannabis moves out of prohibition.
</p>
“Our tech stuff is still considered fairly pedestrian in our industry, but that’s what makes it so exciting of an opportunity.”
<p>
“We see a lot of tech guys, and business guys coming into the cannabis space and they’ve seen it before, they know what is going on. It is just a simple analogy.”
</p>
“You see a lot of marriages between tech and business in the cannabis space that are creating some amazing breakthroughs and developments.” — Alex Rogers
<p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
</p>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>