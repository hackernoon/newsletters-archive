---
id: january-9
title: ' '
---


<h2><a href="https://podcast.hackernoon.com/e/hacking-the-self-with-nick-jankel/">E17 - Hacking The Self with Nick Jankel</a></h2>
<h4>January 9, 2019</h4>

<h4><strong>Episode 17 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with serial entrepreneur and author Nick Seneca Jankel.</strong></h4>

<p>
In this episode Trent Lapinskiand <a href="https://medium.com/u/ec620e841292">Nick Seneca Jankel</a> discuss consciousness hacking, his new book <em><a href="https://www.amazon.com/gp/product/B07G8GWNK4/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B07G8GWNK4&linkCode=as2&tag=bandimusic-20&linkId=136452252a9a3fd3036eaebe2e84aa72">Spiritual Atheist</a></em>, and how to be a spiritual person without giving into religion or ideology.
</p>
“I am hacking capitalism to make a more connected World.”
<p>
“We’ve got to use this technological power, this power of business, creative power, and we’ve got to take on some big problems.”
</p>
“What is purpose? Well, purpose is like love in action. It is that love and kindness that comes out into I’m going to take on this community issue, I’m going to take on a bigger social problem then I was before. Until we can access purpose within, and keep it stable within us, that control and protect mode of a monkey will keep going ‘forget the purpose, lets make another million, that would be really cool, then we’ll be loved’.” —Nick Seneca Jankel
<p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
</p>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>