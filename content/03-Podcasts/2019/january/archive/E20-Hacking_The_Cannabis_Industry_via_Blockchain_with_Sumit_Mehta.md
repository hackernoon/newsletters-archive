---
id: january-24
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/hacking-the-cannabis-industry-via-blockchain-with-sumit%c2%a0mehta/">E20 - Hacking The Cannabis Industry via Blockchain with Sumit Mehta</a></h2>
<h4>January 24, 2019</h4>

<h4><strong>Episode 20 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with Sumit Mehta, CEO and founder of <a href="https://mazakali.com/">Mazakali</a>, a Cannabis Investment Banking Platform.</strong></h4>

<p>
In this episode Trent Lapinskiand Sumit Mehta discuss the emerging cannabis industry, the science behind cannabis’s medicinal benefits, and Sumit’s new blockchain based cannabis investment platform.
</p>
“If we looked back at 1935, and if we were having this conversation then, we might find it fairly ludicrous if someone suggested that we elect a governmental body into power that would put us in cages for having a relationship with a plant.”
<p>
“When we begin to move into a World where we see all of the benefits around our lives, around our communities, around our animals, around our plants, and around our planet that this plant is going to be able to provide I am very excited and very optimistic to be a part of that future; because it is coming, and it is coming quicker then most of us are likely prepared for.” —Sumit Mehta
</p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>