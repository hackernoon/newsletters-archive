---
id: january-22
title: ' '
---


<h2><a href="https://podcast.hackernoon.com/e/hacking-education-for-the-future-with-eric-pm-vermeulen/">E19 - Hacking Education For The Future with Erik P.M. Vermeulen</a></h2>
<h4>January 22, 2019</h4>

<h4><strong>Episode 19 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with <a href="https://medium.com/u/9eaa7a0096d3">Erik P.M. Vermeulen</a>, Professor, Executive, and innovator.</strong></h4>

<p>
<em>Today’s show would not be possible without <a href="https://do.co/2TTnFCL">Digital Ocean</a>. Learn more at <a href="https://do.co/2TTnFCL">do.co/hackernoon</a>.</em>
</p>
In this episode Trent Lapinski and <a href="https://medium.com/u/9eaa7a0096d3">Erik P.M. Vermeulen</a> discuss decentralization, automation, and the future of work for Millennials.
<p>
“I really believe in communities.”
</p>
“When I talk about creativity, and what I want my students to be, I want them to understand communities. I want them to be visible.’”
<p>
“Thinking about how to become visible in a community based World, and how you can actually contribute and build something with the community is something they cannot do on their own. Most of them don’t even understand it, and when we talk about education, that’s what education is about, because it is going to happen, this decentralized thing.” — <a href="https://medium.com/u/9eaa7a0096d3">Erik P.M. Vermeulen</a>
</p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>