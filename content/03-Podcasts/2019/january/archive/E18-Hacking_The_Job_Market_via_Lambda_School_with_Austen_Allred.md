---
id: january-14
title: ' '
---


<h2><a href="https://podcast.hackernoon.com/e/hacking-the-job-market-via-lambda-school-with-austen%c2%a0allred/">E18 - Hacking The Job Market via Lambda School with Austen Allred</a></h2>
<h4>January 14, 2019</h4>

<h4><strong>Episode 18 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with <a href="https://medium.com/u/1e57e704325c">Austen Allred</a>, the CEO and founder of <a href="https://www.lambdaschool.com/">Lambda School</a>.</strong></h4>

<p>
<em>Today’s show would not be possible without Digital Ocean. Get started on DigitalOcean for free with a free $100 credit at <a href="https://do.co/hackernoon">do.co/hackernoon</a>.</em>
</p>
In this episode Trent Lapinski and <a href="https://medium.com/u/1e57e704325c">Austen Allred</a> discuss learning programming and how <a href="https://www.lambdaschool.com/">Lambda School</a> trains software engineers.
<p>
“We want to get the point where 90% of our students are hired within 90-days of graduation.”
</p>
“For us, it is very important that when you graduate from Lambda, we want to say, ‘this person has a stamp of approval they understand these 120 things, and we know that and we verified that and people stand by them.’”
<p>
“In the longterm that will matter a lot when it comes to employers and hiring. They will know what a Lambda graduate means.” — Austen Allred
</p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>