---
id: january-2019
title: January 2019
sidebar_label: January
---

- [Hacking The Self with Nick Jankel](archive/january-9)
- [Hacking The Job Market via Lambda School with Austen Allred](archive/january-14)
- [Hacking Education For The Future with Erik P.M. Vermeulen](archive/january-22)
- [Hacking The Cannabis Industry via Blockchain with Sumit Mehta](archive/january-24)
- [Hacking The Tokenization of Assets with Danny An of TrustToken](archive/january-28)
- [The Cannabis Conference Hack with Alex Rogers](archive/january-29)