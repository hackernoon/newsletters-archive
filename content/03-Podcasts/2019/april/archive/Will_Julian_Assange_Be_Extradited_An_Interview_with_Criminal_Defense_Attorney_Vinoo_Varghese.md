---
id: april11
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/will-julian-assange-be-extradited-an-interview-with-criminal-defense-attorney-vinoo-varghese/">Will Julian Assange Be Extradited? An Interview with Criminal Defense Attorney Vinoo Varghese</a></h2>
<h4>April 11, 2019</h4>

<p>
<strong>A news breaking special episode of the Hacker Noon Podcast: An interview with criminal defense attorney Vinoo Varghese.</strong>
</p>
<p>
"Your question of is Julian Assange going to be extradited to the US? The answer is going to be yes." Vinoo Varghese, Criminal Defense Attorney
</p>
<p>
This is a special episode of the Hacker Noon Podcast. We just wrapped up an interview with criminal defense attorney, Vinoo Varghese, where we spent about 10-minutes discussing the arrest of Wikileaks founder Julian Assange. Due to the timeliness of this story, we decided to release this clip from the episode. The full episode will be released in several weeks, and discusses a range of other topics.
</p>
<p>
Host: Trent Lapinski — <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&v=tvBi4QlaXEo&event=video_description&redir_token=cOn82QVfIxQkcXR8rKf5f5_tY5d8MTU1NTEwOTA2NUAxNTU1MDIyNjY1">https://trentlapinski.com</a> <a href="https://www.youtube.com/redirect?q=http%3A%2F%2Ftwitter.com%2Ftrentlapinski&v=tvBi4QlaXEo&event=video_description&redir_token=cOn82QVfIxQkcXR8rKf5f5_tY5d8MTU1NTEwOTA2NUAxNTU1MDIyNjY1">http://twitter.com/trentlapinski</a>
</p>
Music by Derek Bernard  — <a href="https://www.youtube.com/redirect?q=http%3A%2F%2F%E2%80%8Ahaberdasherband.com%2Fproduction&v=tvBi4QlaXEo&event=video_description&redir_token=cOn82QVfIxQkcXR8rKf5f5_tY5d8MTU1NTEwOTA2NUAxNTU1MDIyNjY1">http:// haberdasherband.com/production</a>