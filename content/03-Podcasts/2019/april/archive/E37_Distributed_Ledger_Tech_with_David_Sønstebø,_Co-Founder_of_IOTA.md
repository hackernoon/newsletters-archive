---
id: april-11
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/distributed-ledger-tech-with-david-s%c3%b8nsteb%c3%b8-co-founder-of-iota/">E37 - Distributed Ledger Tech with David Sønstebø, Co-Founder of IOTA</a></h2>
<h4>April 11, 2019</h4>

<h4><strong>Episode 37 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with David Sønstebø of <a href="http://iota.org/">IOTA</a>, a permission-less distributed ledger for a new economy.</strong></h4>

<p>
<em>This episode of Hacker Noon is sponsored by DigitalOcean. Discover why developers love DigitalOcean and get started with a free $50 credit at <a href="https://l.facebook.com/l.php?u=https%3A%2F%2Fdo.co%2Fhackernoon%3Ffbclid%3DIwAR0rlAqFCLte3_f1pIJLMWlLOZPbjRu6CooU-FoR4-h6eoupceNQJIMLDag&h=AT3ylLdnTJBMQGXK51tG8ipTxCjNE8is7-ZgdJ25PrSH5oXj4ATRlNo0wRenVRK9yyYf4_Eh4Z4-wqOy7TOzNCBd5xx4q7FSehUujYFwevF533P7Xwb_Cmw-E9wIx6e4oobttqfALqMTo_55Bsn8GY-6ds-LVZzSG0bpXfgKAFga6i8K86eEISam5fUji5qhTxqC-ePiDuqaXMT6Q92EXwzvT-EBG3KFdIkKLxId-3heeyKmBpbh2iCkq68omoYnIoMjznPtMz9sqsSKvVpP_sf7bTVSx4oVuCuTSBHH52jRr1yU3VqQjEaX7ZKbqxPVGPdT4oXEwEmZx40p1bPm8BXMem2y2A6p7LvhDkuzeFJ_tmllLDI6BTYU0xPwCIpkQOwxWkqQHk5W9UEBGS6RxNqtTpL3x0utD_oSThsDbdGnB5RQ82SIlC2TRqjSaqP7dV63cwcvhVH4-Ry8_uBUcSvhSz3oJW1pNK5E8VXn8GEfWUrZ6On8JJLPeQhg8eGdhJCUFVRJBuA_x5fgTOTdpAcTz_88ryuFRa5bwG2SbdHRD484N2reIGiEdcU5h8MVQ0Gou_OkG-XTzjIcbl23VNjbeL8lE4GUILSqJr68njEGdGxEUwOLeCQ75aWF_gT6SyxD_Vx6KEo8CluKx88oJVHg">https://do.co/hackernoon</a></em>
</p>
<p>
In this episode Trent Lapinski interviews David Sønstebø, the co-founder of IOTA, you get to learn about distributed technologies, edge computing and what is happening in crypto today!
</p>
<p>
“If you look at how much computational power something like AWS or Azure Microsoft, if you look at how much they have, it sounds impressive, and I’m not saying it isn’t, but when you compare it to the idle amount of computation that exists in all the phones, all the laptops, and all the desktops around the globe it pales in comparison. With this kind of software you can unlock this decentralized AWS, so to speak, that also takes into account the edge. And then of course, your laptop, when it’s idle, you can actually make money from it.”<em> — David </em>Sønstebø
</p>
<p>
<em>Production and music by Derek Bernard — <a href="http://haberdasherband.com/production?fbclid=IwAR2d8t0cNGHRm1ajmUNWKZ-TMUMawREhvIHSy54LKcOElf7v_TOvkAjZ78Y">haberdasherband.com/production</a></em>
</p>
<em>Host: <a href="https://medium.com/u/138c86887e95">Trent Lapinski</a> — <a href="https://trentlapinski.com/">https://trentlapinski.com</a></em>