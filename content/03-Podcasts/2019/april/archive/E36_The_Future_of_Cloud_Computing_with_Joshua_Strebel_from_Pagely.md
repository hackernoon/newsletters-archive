---
id: april-9
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/the-future-of-cloud-computing-with-joshua-strebel-from%c2%a0pagely/">E36 - The Future of Cloud Computing with Joshua Strebel from Pagely</a></h2>
<h4>April 9, 2019</h4>

<h4><strong>Episode 36 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with Joshua Strebel, CEO of <a href="http://pagely.com/">Pagely</a> and <a href="http://northstack.com/">NorthStack</a></strong></h4>

<p>
In this episode Trent Lapinski and Joshua Strebel discuss serverless, cloud computing, devops, WordPress, and Joshua’s new project NorthStack.
</p>
<p>
<em>“You talk about AI and specifically around content publishing, there’s those crazy algorithms now that you can give it a sentence and a closing and it’ll write a thousand words in between and it will be on point. You’re like ‘I couldn’t have written this any better!” — Joshua Strebel</em>
</p>
<p>
Production and music by Derek Bernard — <a href="http://haberdasherband.com/production?fbclid=IwAR2d8t0cNGHRm1ajmUNWKZ-TMUMawREhvIHSy54LKcOElf7v_TOvkAjZ78Y">haberdasherband.com/production</a>
</p>
Host: Trent Lapinski — <a href="https://trentlapinski.com/">https://trentlapinski.com</a>