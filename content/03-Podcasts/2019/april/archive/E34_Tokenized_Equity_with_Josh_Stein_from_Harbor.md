---
id: april-2
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/josh-stein/">E34 - Tokenized Equity with Josh Stein from Harbor</a></h2>
<h4>April 2, 2019</h4>

<h4><strong>Episode 34 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with Josh Stein of <a href="https://harbor.com/">Harbor</a>, a platform for tokenizing investments.</strong></h4>

<p>
In this episode <a href="https://medium.com/@trentlapinski">Trent Lapinski</a>interviews Josh Stein from Harbor, a platform for tokenizing investments.
</p>
<p>
“When we tokenize these shares (using Blockchain) we’re just taking it out of the back office of the company. We’re recording the shares on this giant excel spreadsheet in the sky we call Ethereum, or Blockchain. But it’s got these great properties, which is that it’s public, so you know that if this smart contract represents shares, there’s no double counting of shares. And, you no longer have to go to the company to record the transfer of the shares. Buyer and seller can find each other digitally, they can transact digitally on the Blockchain, and in essence both of them are writing a new entry to the books of records in the company and they’re both signing it digitally.<em>” — Josh Stein</em>
</p>
<em>Production and music by Derek Bernard — <a href="http://haberdasherband.com/production?fbclid=IwAR2d8t0cNGHRm1ajmUNWKZ-TMUMawREhvIHSy54LKcOElf7v_TOvkAjZ78Y">haberdasherband.com/production</a></em>
<em>Host: Trent Lapinski —  <a href="https://trentlapinski.com/">https://trentlapinski.com</a></em>