---
id: april-2019
title: April 2019
sidebar_label: April
---

- [Tokenized Equity with Josh Stein from Harbor](archive/april-2)
- [France in the Blockchain Ecosystem with Karim Sabba from Paris Blockchain Week](archive/april-5)
- [The Future of Cloud Computing with Joshua Strebel from Pagely](archive/april-9)
- [Distributed Ledger Tech with David Sønstebø, Co-Founder of IOTA](archive/april-11)
- [Will Julian Assange Be Extradited? An Interview with Criminal Defense Attorney Vinoo Varghese](archive/april11)
- [Solving Problems in the Tech Community with Pariss Athena of BlackTechTwitter & BTPipeline](archive/april-16)
- [Big Data and Machine Learning with Nick Caldwell](archive/april-18)
- [The Technology and Humanity Interface with Cris Beasley of Hack Reality](archive/april-25)