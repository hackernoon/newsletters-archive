---
id: august-2019
title: August 2019
sidebar_label: August
---

- [Top Articles - Cassie Kozyrkov - Why Businesses Fail At Machine Learning](archive/august-1)
- [$1B in Orders on LeafLink’s Platform with CEO Ryan G. Smith](archive/august-8)
- [We Are Living in the Simulation Hypothesis with Riz Virk](archive/august-13)
- [#CrazyTechStories @ Huckletree, London, England - Set 2](archive/august-20)
- [Hacker Noon’s Editors Talk #noonies: On Voting, Winning, Tweeting, and Cheating](archive/august-22)
- [Banking the Unbanked with Anton Mozgovoy - Former VP of Technology at Humaniq](archive/august-29)