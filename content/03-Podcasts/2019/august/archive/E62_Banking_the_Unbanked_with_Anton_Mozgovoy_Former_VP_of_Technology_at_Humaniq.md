---
id: august-29
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/e63-banking-the-unbanked-with-anton-mozgovoy-former-vp-of-technology-at-humaniq/">E62 - Banking the Unbanked with Anton Mozgovoy - Former VP of Technology at Humaniq</a></h2>
<h4>August 29, 2019</h4>

<p>Episode 62 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with Former VP of Technology at Humaniq, Anton Mozgovoy</p>

<h4><strong>Listen to the interview on <a href="https://itunes.apple.com/us/podcast/product-iteration-with-hacker-noon-interim-cto-dane-lyons/id1436233955?i=1000421970409&mt=2">iTunes</a>, or <a href="https://play.google.com/music/m/Dfuna5a4pzsmjr76bxabkxdrhim?t=Product_Iteration_with_Hacker_Noon_Interim_CTO_Dane_Lyons-Hacker_Noon_Podcast">Google Podcast</a>, or watch on <a href="https://www.youtube.com/channel/UChu5YILgrOYOfkfRlTB-D-g">YouTube</a>.</strong></h4>

<p>
In this episode <a href="http://www.haberdasherband.com/">Derek Bernard</a> interviews Hacker Noon contributing author, <a href="https://mozgovoy.me/">Anton Mozgovoy</a>, former VP of Technology at Humaniq. 
</p>
They discuss his prior project in which the blockchain was utilized and a coin created to bring financial power to the unbanked in Africa; at its peak, they were 500,000 members strong. They also get a bit into the challenges facing the growing generational divide being accelerated by the pace of advancing technology, as well as the global forces at play in the technology space.
<p>
“There are actually close to 2 billion people on the planet that don’t have access to the banking system. They’re still using cash; they’re still trading with each other. It’s just that they’re not trading on a global scale. It means that they can send money [over] a distance.”
</p>
“That’s just one simple use case if you’d applied to many different models out there. One of the biggest ones that blockchain people really refer to which is Metcalf’s Law that tells you that the power of the system is quadratically correlated to the number of people that are involved in it and then they like to apply it to: what would happen if we could get those 2 billion that are right now excluded and include them into this global eco-system.”
<p>
<em> —<strong> </strong>Anton Mozgovoy</em>
</p>
<em>P.S. If you dig the new <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>, consider <a href="https://itunes.apple.com/us/podcast/the-hacker-noon-podcast/id1436233955?mt=2">giving us a 5 star review on iTunes.</a></em>
<h4>Also check out this month’s <a href="https://hackernoon.com/archive/2019/07">top stories</a>,<strong> </strong><a href="https://hackernoon.com/latest-tech-stories/home">the latest stories</a>, and <a href="http://hackernoon.com/">today’s homepage</a>.</h4>