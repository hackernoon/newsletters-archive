---
id: august-20
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/crazytechstories-huckletree-london-england-1566332997/">#CrazyTechStories @ Huckletree, London, England - Set 2</a></h2>
<h4>August 20, 2019</h4>

<p>Where Hacker Noon brings you a handful of stories from the community at large.</p>

<p>Сrazy tech stories is part of a larger series of talks presented in association with Indeed Prime and Huckletree at Huckletree’s London location.</p>

<p>
This episode of Hacker Noon is sponsored by Indeed Prime. 
</p>
<p>
Visit<a href="https://www.youtube.com/redirect?event=video_description&v=WVmOhq3pSY8&redir_token=hd0u4FTcrgdOtj4VLXpVVHgVQoZ8MTU2MjI2NjIxOUAxNTYyMTc5ODE5&q=https%3A%2F%2Fwww.indeedprime.com%2Fhackernoon%2F"> https://www.indeedprime.com/hackernoon/</a> to flip the script on the job search and join now for access to resumé reviews, 1:1 sessions with technical career coaches, personalized work-style assessments and even negotiation tips to help seal the deal.
</p>
<p>In this episode you get to meet <a href="https://hackernoon.com/@priansh">Priansh Shah</a>, co-founder of <a href="https://helloaiko.com/">Aiko AI</a>, <a href="https://hackernoon.com/@david">David Smooke</a>, <a href="https://hackernoon.com/">CEO of Hacker Noon</a>, <a href="https://hackernoon.com/@anthonyrose">Anthony Rose</a>, Founder and CEO of <a href="https://seedlegals.com/">SeedLegals</a>, and <a href="https://hackernoon.com/@Webb">Thomas Webb</a>, <a href="https://webb.site/">artist/hacker extraordinaire</a>. </p>


<h4><strong>Listen to the talks on <a href="https://podcasts.apple.com/us/podcast/crazytechstories-huckletree-london-england/id1436233955?i=1000443502826">iTunes</a> or watch on <a href="https://www.youtube.com/channel/UChu5YILgrOYOfkfRlTB-D-g">YouTube</a>.</strong></h4>


<p>
Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a>
</p>
<a href="https://hackernoon.com/">https://hackernoon.com/</a> 
<p>
<a href="https://community.hackernoon.com/">https://community.hackernoon.com/</a>  
</p>
<a href="https://contribute.hackernoon.com/">https://contribute.hackernoon.com/</a> 
<p>
<a href="https://sponsor.hackernoon.com/">https://sponsor.hackernoon.com/</a> 
</p>
<a href="https://podcast.hackernoon.com/">https://podcast.hackernoon.com/</a> 
<p>
<a href="https://twitter.com/hackernoon/">https://twitter.com/hackernoon/</a> 
</p>
<a href="https://facebook.com/hackernoon/">https://facebook.com/hackernoon/</a> 
<em>P.S. If you dig the new <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>, consider <a href="https://itunes.apple.com/us/podcast/the-hacker-noon-podcast/id1436233955?mt=2">giving us a 5 star review on iTunes.</a></em>
<h4>Also check out the <a href="https://hackernoon.com/archive/2019/07">top stories from this month</a>,<strong> </strong><a href="https://hackernoon.com/latest-tech-stories/home">the latest stories</a>, and <a href="http://hackernoon.com/">today’s homepage</a>.</h4>