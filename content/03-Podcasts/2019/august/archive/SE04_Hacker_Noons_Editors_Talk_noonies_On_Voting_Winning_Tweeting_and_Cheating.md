---
id: august-22
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/e62-hacker-noons-editors-talk-noonies-on-voting-winning-tweeting-and-cheating/">SE04 - Hacker Noon’s Editors Talk #noonies: On Voting, Winning, Tweeting, and Cheating</a></h2>
<h4>August 22, 2019</h4>

<p>Special Episode 04 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: Join Hacker Noon Editor’s Natasha Nel and Storm Farrell as they converse with Hacker Noon CEO David Smooke on everything Noonies.</p>


<h4><strong>Listen to the interview on <a href="https://itunes.apple.com/us/podcast/product-iteration-with-hacker-noon-interim-cto-dane-lyons/id1436233955?i=1000421970409&mt=2">iTunes</a>, or <a href="https://play.google.com/music/m/Dfuna5a4pzsmjr76bxabkxdrhim?t=Product_Iteration_with_Hacker_Noon_Interim_CTO_Dane_Lyons-Hacker_Noon_Podcast">Google Podcast</a>, or watch on <a href="https://www.youtube.com/channel/UChu5YILgrOYOfkfRlTB-D-g">YouTube</a>.</strong></h4>
 
<p>
In this episode <a href="https://hackernoon.com/@david">David Smooke</a> interviews Hacker Noon Editor’s <a href="https://www.hackernoon.com/@natasha">Natasha Nel</a> and <a href="https://hackernoon.com/@Storm">Storm Farrell</a> to discuss the Tech Industry’s Greenest Awards, the noonies!  
</p>
<p>
They cover what went right, what went wrong, as well as some of the noonies biggest winners.
</p>
 
<p>
“When we were conceptualizing this idea for the awards voting/polling situation, we didn’t really find any obvious, simple voting or polling apps. And as far as we could tell, none of the solutions allowed us to set up a situation where there was a very light touch voting opportunity for users, so we opted to build one ourselves”
</p>
<em> —<strong> </strong>Storm Farrell</em>
<p>
“The tech industries greenest awards! The noonies were built to recognize perfectly the best and worst people and products of the internet, and indeed they were.”
</p>
“Thank you to the community for taking part. There were a lot of who were really enthusiastic, and I’m grateful.”
<p>
<em> —<strong> </strong>Natasha Nel</em>
</p>
“There’s areas where we can go further with custom voting and how what we choose to build with the future of the product is it’s kind of interesting to think about how content is shared, and then when it’s curated well, how is it shared more.”
<p>
<em> —<strong> </strong>David Smooke</em>
</p>
<em>P.S. If you dig the new <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>, consider <a href="https://itunes.apple.com/us/podcast/the-hacker-noon-podcast/id1436233955?mt=2">giving us a 5 star review on iTunes.</a></em>
<h4>Also check out this month’s <a href="https://hackernoon.com/archive/2019/07">top stories</a>,<strong> </strong><a href="https://hackernoon.com/latest-tech-stories/home">the latest stories</a>, and <a href="http://hackernoon.com/">today’s homepage</a>.</h4>