---
id: june-7
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/e49-quality-assurance-and-the-future-of-society-with-fred-stevens-smith/">E49 - Quality Assurance and the Future of Society with Fred Stevens-Smith</a></h2>
<h4>June 7, 2019</h4>

<p>
Episode 49 of the Hacker Noon Podcast: An interview with Fred Stevens-Smith, CEO at Rainforest QA
</p>
This episode of Hacker Noon is sponsored by DigitalOcean. Discover why developers love DigitalOcean and get started with a free $50 credit at <a href="https://do.co/hackernoon">https://do.co/hackernoon</a>
<p>
In this episode Trent Lapinski interviews Fred Stevens-Smith, CEO at Rainforest QA. In it is discussed quality assurance, automation and the threat to human jobs, and the ramifications for society. 
</p>
<em>"Most people don't really have the tools to understand how software really works. So it becomes this question like whether robots going to take my job or not?" - Fred Stevens-Smith</em>
<p>
Production and music by Derek Bernard - <a href="http://www.haberdasherband.com/production">www.haberdasherband.com/production</a>
</p>
Host: Trent Lapinski - <a href="https://trentlapinski.com/">https://trentlapinski.com</a>
<p>
<a href="https://hackernoon.com/">https://hackernoon.com/</a>
</p>
<a href="https://community.hackernoon.com/">https://community.hackernoon.com/</a>
<p>
<a href="https://contribute.hackernoon.com/">https://contribute.hackernoon.com/</a>
</p>
<a href="https://sponsor.hackernoon.com/">https://sponsor.hackernoon.com/</a>
<p>
<a href="https://podcast.hackernoon.com/">https://podcast.hackernoon.com/</a>
</p>
<a href="https://twitter.com/hackernoon/">https://twitter.com/hackernoon/</a>
<a href="https://facebook.com/hackernoon/">https://facebook.com/hackernoon/</a>