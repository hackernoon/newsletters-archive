---
id: june-13
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/e51-the-community-of-hacker-noon/">E51 - Hacker Noon From a Community Perspective with Linh Dao Smooke, COO of Hacker Noon</a></h2>
<h4>June 13, 2019</h4>

<p>
This episode of Hacker Noon is sponsored by DigitalOcean. Discover why developers love DigitalOcean and get started with a free $50 credit at<a href="https://www.youtube.com/redirect?redir_token=AHgv2AHCk-VJJ1BchBP6L0x8Qhl8MTU2MDU0ODkyNkAxNTYwNDYyNTI2&q=https%3A%2F%2Fdo.co%2Fhackernoon&event=video_description&v=gGdm68uunEA"> https://do.co/hackernoon</a>
</p>
<h3>Episode 51 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with <a href="https://hackernoon.com/@linhdaosmooke">Linh Dao Smooke</a>, COO of <a href="https://hackernoon.com/">Hacker Noon.</a></h3>


<h4><strong>Listen to the interview on <a href="https://podcasts.apple.com/us/podcast/e51-hacker-noon-from-community-perspective-lihn-dao/id1436233955?i=1000441440735">iTunes</a>, or <a href="https://play.google.com/music/m/Dfuna5a4pzsmjr76bxabkxdrhim?t=Product_Iteration_with_Hacker_Noon_Interim_CTO_Dane_Lyons-Hacker_Noon_Podcast">Google Podcast</a>, or watch on <a href="https://youtu.be/BjpGihnicIg">YouTube</a>.</strong></h4>


<p>
In this episode <a href="https://hackernoon.com/@linhdaosmooke">Linh Dao Smooke</a>, COO of <a href="https://hackernoon.com/">Hacker Noon</a>, and <a href="https://castig.org/">Chris Castig</a>, host of the Learn How to Code podcast, discuss Hacker Noon's new trajectory from June 2019 and Hacker Noon from a community perspective. Stick around and find out more!
</p>
<em>“Tech it's basically electricity right now.  It's everywhere; it's with everything that we do. So if you have a story that is related to tech we always and only judge the story on its own merit. It doesn't matter if you've never published with us before. It doesn't matter if you've never published anywhere before.”</em>
<p>
<em>"We are looking at Hacker Noon content as having three niches. We have the blockchain, bitcoin, cryptocurrency people. We have the general tech, startup people. And then we have software development. Software is where we started, so it's always dear to our heart."</em>
</p>
<em>- Linh Dao Smooke</em>
<p>
Production and music by Derek Bernard - <a href="http://haberdasherband.com/production?fbclid=IwAR2d8t0cNGHRm1ajmUNWKZ-TMUMawREhvIHSy54LKcOElf7v_TOvkAjZ78Y">haberdasherband.com/production</a>
</p>
Host: Chris Castiglione - <a href="https://learn.onemonth.com/learn-to-code-podcast/">https://learn.onemonth.com/learn-to-code-podcast/</a> 
<h4>Also check out the <a href="https://hackernoon.com/archive/2019/06">top stories from June</a>,<strong> </strong><a href="https://hackernoon.com/latest-tech-stories/home">the latest stories</a>, and <a href="http://hackernoon.com/">today’s homepage</a>.</h4>