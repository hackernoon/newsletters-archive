---
id: june-18
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/e52-mahbod-moghadam/">E52 - The Future of Centralized Internet with Mahbod Moghadam</a></h2>
<h4>June 18, 2019</h4>

<p>
Episode 52 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with serial entrepreneur <a href="https://hackernoon.com/@mahbodmoghadam">Mahbod Moghadam</a>, co-founder of <a href="https://genius.com/">GENIUS</a> and co-founder and CCO of <a href="https://everipedia.org/">Everipedia</a>
</p>
<h4><strong>Listen to the interview on <a href="https://itunes.apple.com/us/podcast/product-iteration-with-hacker-noon-interim-cto-dane-lyons/id1436233955?i=1000421970409&mt=2">iTunes</a> or watch on <a href="https://youtu.be/Aayu3SHrhMQ">YouTube</a>.</strong></h4>


<p>
In this episode Trent Lapinski interviews <a href="https://hackernoon.com/@mahbodmoghadam">Mahbod Moghadam</a>, co-founder of <a href="https://genius.com/">GENIUS</a> and co-founder and CCO of <a href="https://everipedia.org/">Everipedia</a>, wiki-based online encyclopedia. You get to discover the future of the Internet, blockchain and current projects Mahbod is working on.
</p>
"The blockchain stuff, the plan seems to be to destroy the centralized Internet. And some people in the centralized Internet seem to be showing love".
<p>
"Are they trying to co-opt this? Like the 10 million dollars question is if someone going to destroy Facebook? Or is Facebook just going to co-opt whatever someone else builds like they did with Snapchat?"
</p>
"My mission in life is to get people to come out of their privacy. You embrace digital real estate”.
<p>
<em>—<strong> </strong>Mahbod Moghadam</em>
</p>
Production and music by Derek Bernard - <a href="http://haberdasherband.com/production?fbclid=IwAR2d8t0cNGHRm1ajmUNWKZ-TMUMawREhvIHSy54LKcOElf7v_TOvkAjZ78Y">haberdasherband.com/production</a>
<p>
Host: Trent Lapinski - <a href="https://trentlapinski.com/">https://trentlapinski.com</a>
</p>
<em>P.S. If you dig the new <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>, consider <a href="https://itunes.apple.com/us/podcast/the-hacker-noon-podcast/id1436233955?mt=2">giving us a 5 star review on iTunes.</a></em>
Also check out the <a href="https://hackernoon.com/archive/2019/06">top stories from June</a>, <a href="https://hackernoon.com/latest-tech-stories/home">the latest stories</a>, and <a href="http://hackernoon.com/">today’s homepage</a>