---
id: june-4
title: ' '
---

<h3><a href="https://podcast.hackernoon.com/e/e48-cheryl-contee/">E48 - Stages of Company Building: From Startup to Growth and Exit with Cheryl Contee</a></h3>
<h4>June 4, 2019</h4>

<h3>Episode 48 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with Cheryl Contee, CEO at <a href="https://dobigthings.today/">Do Big Things</a> and Author of Mechanical Bull: How You Can Achieve Startup Success.</h3>
<h4>This episode of Hacker Noon is sponsored by Indeed Prime.</h4>

<h4>Visit <a href="https://www.indeedprime.com/hackernoon/">https://www.indeedprime.com/hackernoon/</a> to flip the script on the job search and join now for access to resumé reviews, 1:1 sessions with technical career coaches, personalized work-style assessments and even negotiation tips to help seal the deal.</h4>

<h4><strong>Listen to the interview on <a href="https://podcasts.apple.com/us/podcast/hacker-noon-podcast/id1436233955">iTunes</a>, or <a href="https://play.google.com/music/m/Dfuna5a4pzsmjr76bxabkxdrhim?t=Product_Iteration_with_Hacker_Noon_Interim_CTO_Dane_Lyons-Hacker_Noon_Podcast">Google Podcast</a>, or watch on <a href="https://youtu.be/P6evebMeXhU">YouTube</a>.</strong></h4>

<h4>In this episode  <a href="https://hackernoon.com/@crisbeasley">Cris Beasley</a> interviews Cheryl Contee, co-founder of social marketing software Attentive.ly at Blackbaud, the first tech startup with a black female founder on board in history to be acquired by a NASDAQ-traded company. You get to learn essential stages of successful company building and how to combine the career of entrepreneur with motherhood.</h4>

<h4><em>"There is no job, there is no product, there is no service that doesn't involve technology going forward in the 21 century.”</em></h4>

<h4><em>“Be yourself and find the investors who really get you especially if you don't come from a technical background, if you are a woman, if you are a minority. You might have to knock on more doors. I certainly did. I have to knock on a lot more doors to finally find those investors who really got it. ” — Cheryl Contee</em></h4>

<h4>Production and music by Derek Bernard - <a href="http://haberdasherband.com/production?fbclid=IwAR2d8t0cNGHRm1ajmUNWKZ-TMUMawREhvIHSy54LKcOElf7v_TOvkAjZ78Y">haberdasherband.com/production</a></h4>

<h4>Host: Cris Beasley - <a href="https://hackernoon.com/@crisbeasley">https://hackernoon.com/@crisbeasley</a></h4>

<h4><em>P.S. If you dig the new <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>, consider <a href="https://itunes.apple.com/us/podcast/the-hacker-noon-podcast/id1436233955?mt=2">giving us a 5 star review on iTunes.</a></em></h4>

<h4>Also check out the <a href="https://hackernoon.com/archive/2019/06">top stories from June</a>,<strong> </strong><a href="https://hackernoon.com/latest-tech-stories/home">the latest stories</a>, and <a href="http://hackernoon.com/">today’s homepage</a>.</h4>