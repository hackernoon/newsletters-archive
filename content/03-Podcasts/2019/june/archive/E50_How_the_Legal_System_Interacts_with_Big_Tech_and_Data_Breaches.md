---
id: june-11
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/e50-how-the-legal-system-interacts-with-big-tech-and-data-breaches/">E50 - How the Legal System Interacts with Big Tech and Data Breaches</a></h2>
<h4>June 11, 2019</h4>

<p>
Episode 50 of the Hacker Noon Podcast: An interview with Vinoo Varghese, a criminal defense attorney at Varghese & Associates, P.C. and a former prosecutor. 
</p>
This episode of Hacker Noon is sponsored by DigitalOcean. Discover why developers love DigitalOcean and get started with a free $50 credit at <a href="https://do.co/hackernoon">https://do.co/hackernoon</a> 
<p>
In this episode Trent Lapinski interviews Vinoo Varghese, a criminal defense attorney and a former prosecutor. You get to discover more about the war on drugs, cannabis legalization, big tech and data  as well as what exactly happened with #WikiLeaks. 
</p>
“With Facebook next thing that come up they are sharing all the information with Chinese companies.”
<p>
“You've got this very interesting interplay between the criminal illegal system and the Congress trying to figure out what to do. And ultimately you actually come back to a constitutional issue.” —  Vinoo Varghese
</p>
Production and music by Derek Bernard - <a href="http://www.haberdasherband.com/production">www.haberdasherband.com/production</a> 
<p>
Host: Trent Lapinski - <a href="https://trentlapinski.com/">https://trentlapinski.com</a> 
</p>
<a href="https://hackernoon.com/">https://hackernoon.com/</a> 
<p>
<a href="https://community.hackernoon.com/">https://community.hackernoon.com/</a> 
</p>
<a href="https://contribute.hackernoon.com/">https://contribute.hackernoon.com/</a> 
<p>
<a href="https://sponsor.hackernoon.com/">https://sponsor.hackernoon.com/</a> 
</p>
<a href="https://podcast.hackernoon.com/">https://podcast.hackernoon.com/</a> 
<p>
<a href="https://twitter.com/hackernoon/">https://twitter.com/hackernoon/</a> 
</p>
<a href="https://facebook.com/hackernoon/">https://facebook.com/hackernoon/</a> 