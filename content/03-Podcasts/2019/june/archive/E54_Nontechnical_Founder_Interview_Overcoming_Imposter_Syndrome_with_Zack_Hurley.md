---
id: june-25
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/e54-zack-hurley/">E54 - Nontechnical Founder Interview: Overcoming Imposter Syndrome with Zack Hurley</a></h2>
<h4>June 25, 2019</h4>

<p>Episode 54 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with Zack Hurley Co-Founder and CEO of <a href="https://indiesource.com/">Indie Source</a>, entrepreneur with experience working with private and public sector organizations in the fields of business consulting for sales and marketing strategy, operational logistics. </p>


<h4><strong>Listen to the interview on <a href="https://itunes.apple.com/us/podcast/product-iteration-with-hacker-noon-interim-cto-dane-lyons/id1436233955?i=1000421970409&mt=2">iTunes</a> or watch on <a href="https://youtu.be/WVmOhq3pSY8">YouTube</a>.</strong></h4>

<p>
In this episode <a href="https://hackernoon.com/@pmm621">Patrick Murray</a> interviews <a href="https://www.linkedin.com/in/zacharyhurley/">Zack Hurley</a> from <a href="https://indiesource.com/">Indie Source</a>. You get to discover what it takes to become a successful entrepreneur, and how to overcome the imposter syndrome while building new business.
</p>
<em>“Living the 4 hour work week was mind blowing to me”. </em>
<p>
<em>“I finally understood that you don't need to spend 10 or 20 years in a corporation to be an entrepreneur and starting your own thing. You just flipped everything that I knew about what I had to do on a tech. I don't have to wait”. —<strong> </strong>Zack Hurley</em>
</p>
Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a>
<p>
Host:<a href="https://www.linkedin.com/in/patrickmichaelmurray/"> Patrick Murray </a>
</p>
<em>P.S. If you dig the new <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>, consider <a href="https://itunes.apple.com/us/podcast/the-hacker-noon-podcast/id1436233955?mt=2">giving us a 5 star review on iTunes.</a></em>
<h4>Also check out the <a href="https://hackernoon.com/archive/2019/06">top stories from June</a>,<strong> </strong><a href="https://hackernoon.com/latest-tech-stories/home">the latest stories</a>, and <a href="http://hackernoon.com/">today’s homepage</a>.</h4>