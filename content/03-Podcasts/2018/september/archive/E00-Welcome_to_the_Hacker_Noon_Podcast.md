---
id: september-13
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/welcome-to-the-hacker-noon-podcast-1536857888/">E00 - Welcome to the Hacker Noon Podcast!</a></h2>
<h4>September 13, 2018</h4>
<p>
Video of David Smooke, CEO and founder of Hacker Noon, introduces Trent Lapinski the new host for the Hacker Noon Podcast.
</p>
<em>Music and Production by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>