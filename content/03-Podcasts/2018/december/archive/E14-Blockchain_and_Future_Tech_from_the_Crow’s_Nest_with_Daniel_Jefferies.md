---
id: december-6
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/blockchain-and-future-tech-from-the-crows-nest-with-daniel-jefferies/">E14 - Blockchain and Future Tech from the Crow’s Nest with Daniel Jefferies</a></h2>
<h4>December 6, 2018</h4>

<h4><strong>Episode 14 of the <a href="https://podcast.hackernoon.com/">Hacker Noon Podcast</a>: An interview with Science Fiction author, and futurist <a href="https://medium.com/u/618a7c78c957">Daniel Jeffries</a>.</strong></h4>

<p>
In this episode <a href="https://medium.com/u/138c86887e95">Tre</a>n<a href="https://medium.com/u/138c86887e95">t Lapinski</a> and Daniel Jeffries discuss cryptocurrency, blockchain, AI, future technology, and philosophy.
</p>
“We need to stop throwing the damn baby out with the bathwater, and assume that decentralized everything just works because it’s decentralized.”
<p>
“These things that we have now, they are still in their earliest phase. We barely understand them. They are going to evolve into something so mind boggling different from what we have today.”
</p>
“We have to look to each structure, and abstract out the things that work and create new mitigation systems that solve the problems of the existing ones, and when we make the next iteration we stand on the shoulders of giants.” — Daniel Jeffries
<p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
</p>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>