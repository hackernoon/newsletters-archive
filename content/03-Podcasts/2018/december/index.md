---
id: december-2018
title: December 2018
sidebar_label: December
---

- [The VC Approach In Today’s Tech Markets with Steve Schlafman](archive/december-3)
- [Blockchain and Future Tech from the Crow’s Nest with Daniel Jefferies](archive/december-6)
- [Emerging Technologies with Venture Capitalist Julian Moncada](archive/december-12)
- [Building A Movement Through Emerging Tech with Enigma’s Tor Bair](archive/december-19)



<!-- - [](archive/) -->