---
id: november-16
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/the-elusive-black-swan-with-david%c2%a0kadavy/">E11 - The Elusive Black Swan with David Kadavy</a></h2>
<h4>November 16, 2018</h4>

<h4><strong>Episode 11: An interview with creative entrepreneur, best selling author, and podcaster <a href="https://medium.com/u/5377a93ef640">David Kadavy</a></strong></h4>

<p>
In this episode Trent Lapinski and David Kadavy discuss elusive black swam moments, which are moments that describe an event that comes as a surprise and have a major effect.
</p>
“I had an event where my e-mail list went from 5,000 to 30,000 within a couple weeks. All these events, they kind of happened, and I did things to make them happen, but I couldn’t have predicted that they were going to happen. So I wondered to myself, how can you make black swans happen?”
<p>
“They suffer from interacting with an issue I call irrational rationality. Which is the treating of absence of evidence as evidence of absence.” 
</p>
“It always really turned me off, the whole A/B testing obsession that there was in Silicon Valley… Well yeah, if you can use machine learning to do that, that’s even more reasons for you to try and ignore those activities. Then look for the activities that are low investment, but uncertain in outcome. That uncertain outcome may be a very positive outcome.” — David Kadavy
<p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
</p>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>