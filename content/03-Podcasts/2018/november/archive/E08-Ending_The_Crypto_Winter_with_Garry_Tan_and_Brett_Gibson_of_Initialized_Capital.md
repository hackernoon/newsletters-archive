---
id: november-1
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/ending-the-crypto-winter-with-garry-tan-and-brett-gibson-of-initialized-capital/">E08 - Ending The Crypto Winter with Garry Tan and Brett Gibson of Initialized Capital</a></h2>
<h4>November 1, 2018</h4>

<p>
<strong>Episode 8: In this episode we sit down with Garry Tan and Brett Gibson of Initialized Capital and discuss emerging technologies such as cryptocurrency, blockchain, and what sets them apart from other venture capital firms.</strong>
</p>
"People are conditioned for 200 milliseconds at this point. Even waiting 5-seconds for a little spinner is not going to cut it."- Garry Tan
<p>
"Privacy is going to be a big deal for the blockchain and crypto technologies." - Brett Gibson
</p>
"It really is about those users, and what's their experience. We want to help other fellow hackers who are builders." - Garry Tan
<p>
Today's show would not be possible without DataDog, a modern monitoring and analytics solution for any stack, any app, at any scale, anywhere. Visit DataDog.com and get a free t-shirt now. <a href="https://www.youtube.com/redirect?redir_token=0f2jrd8FQ-Sy9JB3h2t8O4HRVHZ8MTU0MTE0MjYwMUAxNTQxMDU2MjAx&event=video_description&v=J_SQK5XDIts&q=http%3A%2F%2Fbit.ly%2F2CTelt7">http://bit.ly/2CTelt7</a>
</p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>