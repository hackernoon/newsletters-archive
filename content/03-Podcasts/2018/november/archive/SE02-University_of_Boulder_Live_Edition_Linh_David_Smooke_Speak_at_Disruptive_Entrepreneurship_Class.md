---
id: november-27
title: ' '
---


<h2><a href="https://podcast.hackernoon.com/e/university-of-boulder-live-edition-linh-david-smooke-speak-at-disruptive-entrepreneurship-class/">SE02 - University of Boulder Live Edition: Linh & David Smooke Speak at Disruptive Entrepreneurship Class</a></h2>
<h4>November 27, 2018</h4>
<p>
Hacker Noon CEO <a href="https://www.davidsmooke.net/">David Smooke</a> & COO <a href="https://www.linkedin.com/in/linhdaosmooke/">Linh Dao Smooke</a> recently spoke at University of Colorado Boulder's Disruptive Entrepreneurship class taught by Professor & Hacker Noon contributing writer <a href="https://hackernoon.com/@ntnsndr">Nathan Schneider.</a> Learn more about <a href="https://www.startengine.com/hackernoon">Hacker Noon's equity crowdfunding campaign</a>. 
</p>
"On the internet right now, there is a massive battle going on between centralization and decentralization." - David
<p>
"It's what drives us everyday: we know that people want to read more, write more and that people rally behind us when we are threatened by an external source." - Linh 
</p>
"Know that the obstacles are only a day, and tomorrow will be a new day and that obstacle won't be as bad, or maybe it'll be worse and day after will be better." -David
<p>
"It's the community that decides what we're gonna' look like - therefore we think it's the best reflection of the internet." -Linh 
</p>
"We're people that built a company out of iteration, and we did not intend to make one of the most popular tech blogs." - David
<p>
"How can we reflect the internet? The merits should be the story and the story itself." - Linh
</p>
Learn more about <a href="https://www.startengine.com/hackernoon">Hacker Noon's equity crowdfunding campaign</a>.
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>