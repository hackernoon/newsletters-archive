---
id: november8
title: ' '
---


<h2><a href="https://podcast.hackernoon.com/e/health-hacks-with-casey-fenton-of-upstock-and-couchsurfing/">E10 - Health Hacks with Casey Fenton of UpStock and CouchSurfing</a></h2>
<h4>November 8, 2018</h4>

<p>
<strong>Episode 10: Part 2 of an interview with Casey Fenton, CEO and Founder of UpStock (<a href="https://www.youtube.com/redirect?redir_token=E4I9o_docdhEZOmAVO3DdLfFoDZ8MTU0MTgyMDYwM0AxNTQxNzM0MjAz&v=rg2MTT8tq1o&q=http%3A%2F%2Fupstock.io&event=video_description">http://upstock.io</a>), and Co-Founder of CouchSurfing (<a href="https://www.youtube.com/redirect?redir_token=E4I9o_docdhEZOmAVO3DdLfFoDZ8MTU0MTgyMDYwM0AxNTQxNzM0MjAz&v=rg2MTT8tq1o&q=http%3A%2F%2Fcouchsurfing.org&event=video_description">http://couchsurfing.org</a>). Listen to Episode 9 for part 1 of this interview on Ego Hacking.</strong>
</p>
In this episode Trent and Casey discuss health hacks, how to live longer, and how to improve your overall health with simple tips and tricks.
<p>
"If you can get someone in your life who loves you, to help you hack yourself… to get you to think 'I care about my health, I care about my health, I want to live a healthy way for a long time.' Get someone to help you get that kernel, that little nugget that is health hacking, and that will grow." 
</p>
"You will start to see all little things in your environment, all over the place, that corroborate and bolster of this identity that 'I care about my health, and I love myself,' and all that good stuff."- Casey Fenton
<p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
</p>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>