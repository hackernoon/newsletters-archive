---
id: october-11
title: ' '
---


<h2><a href="https://podcast.hackernoon.com/e/the-truth-about-crypto-pr-with-kelley-weaver/">E03 - The Truth About Crypto PR with Kelley Weaver</a></h2>
<h4>October 11, 2018</h4>

<p>
Episode 3: An interview with Kelley Weaver, CEO of <a href="https://www.youtube.com/redirect?v=dOr9TPq2HEs&event=video_description&redir_token=9szqo2aNcbPyc4YDnxi5EBqv7gR8MTUzOTMyODQzMkAxNTM5MjQyMDMy&q=https%3A%2F%2Fwww.melrosepr.com%2F">https://www.melrosepr.com/</a>. In this episode we discuss cryptocurrency, blockchain, marketing, PR techniques, and life hacks.
</p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>