---
id: october11
title: ' '
---

<h2><a href="https://podcast.hackernoon.com/e/decentralizing-the-cloud-with-greg-osuri/">E04 - Decentralizing the Cloud with Greg Osuri</a></h2>
<h4>October 11, 2018</h4>

<p>
Episode 4: An interview with Greg Osuri founder of <a href="https://www.youtube.com/redirect?q=http%3A%2F%2FAkash.network&redir_token=jAz6YQkLNytfXHv9ldSsmgKS3_58MTUzOTMyODUyM0AxNTM5MjQyMTIz&event=video_description&v=Nvj2ruVPfYQ">http://Akash.network</a> and Overclock Labs. In this episode we discuss DevOps, and what it takes to scale and run a decentralized infrastructure network.
</p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>