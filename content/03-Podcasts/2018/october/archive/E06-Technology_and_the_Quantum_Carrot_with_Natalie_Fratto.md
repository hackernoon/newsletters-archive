---
id: october18
title: ' '
---


<h2><a href="https://podcast.hackernoon.com/e/technology-and-the-quantum-carrot-with-natalie-fratto/">E06 - Technology and the Quantum Carrot with Natalie Fratto</a></h2>
<h4>October 18, 2018</h4>
<p>
Episode 6: An interview with Natalie Fratto, VP at Silicon Valley Bank. In this episode we discuss everything from quantum computing, artificial intelligence, to adaptability.
</p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>