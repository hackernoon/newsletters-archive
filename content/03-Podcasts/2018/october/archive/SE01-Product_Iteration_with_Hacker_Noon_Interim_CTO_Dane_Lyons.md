---
id: october-16
title: ' '
---


<h2><a href="https://podcast.hackernoon.com/e/product-iteration-with-hacker-noon-interim-cto-dane-lyons/">SE01 - Product Iteration with Hacker Noon Interim CTO Dane Lyons</a></h2>
<h4>October 16, 2018</h4>

<p>
On this special edition episode, <a href="https://www.hackernoon.com/">Hacker Noon</a> Founder & CEO <a href="https://www.davidsmooke.net/">David Smooke</a> guest hosts to discuss the process of making great products with newly hired interim CTO <a href="https://twitter.com/duilen?lang=en">Dane Lyons</a>. Dane is the founder of v1labs & Knowtify, and he is one of the best product minds around. 
</p>
"To build a good product, you've got to embrace change," says Dane Lyons. "When you have a product and you change something about the product, there's really only three outcomes. It's going to get better, it's going to get worse, or it's going to stay the same. "
<p>
Read the <a href="https://hackernoon.com/product-iteration-with-hacker-noon-interim-cto-dane-lyons-9ce7ec3cef55">full transcript on Hacker Noon</a>. Listen to more <a href="https://podcast.hackernoon.com/">Hacker Noon podcasts</a> on <a href="https://itunes.apple.com/us/podcast/hacker-noon-podcast/id1436233955?mt=2">iTunes </a>and <a href="https://www.youtube.com/c/HackerNoon">Youtube</a>.  
</p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>