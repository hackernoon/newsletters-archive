---
id: october-18
title: ' '
---


<h2><a href="https://podcast.hackernoon.com/e/health-insurance-and-blockchain-with-nick-soman/">E05 - Health Insurance and Blockchain with Nick Soman</a></h2>
<h4>October 18, 2018</h4>

<p>
Episode 5: An interview with Nick Soman, CEO of Decent.com, on how to fix the healthcare industry using blockchain technology.
</p>
<em>Production and music by Derek Bernard - <a href="https://haberdasherband.com/production">https://haberdasherband.com/production</a></em>
<em>Host: Trent Lapinski - <a href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftrentlapinski.com&event=video_description&v=qKq-hi-AoH8&redir_token=yl-d2oX1VrQZk4haKt1ozUL9Q8l8MTU1MjUwNjc2OUAxNTUyNDIwMzY5">https://trentlapinski.com</a></em>